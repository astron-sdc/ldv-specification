# Update ldv-spec-db with new ASTROWISE data
(nv 11 oct 2024)

This is a description of how the `ldv-spec-db` database was updated with new dataproducts.
It is mostly based on the description in MIGRATION.md, but additional work is required because.
- (1) the existing and new astrowise schema's needs to be combined first
- (2) the `ldv-spec-db` on production does not run in a Docker container, but bare metal on `sdc-db.astron.nl`


### Pre-requisites
- (sudo) access to the `sdc-db.astron.nl` machine (dop822), through a ssh tunnel like `sdco@dop814.astron.nl`.
  - credentials for Postgres users `ldvadmin` and `ldv_spec` to access the `ldvadmin` and `ldv-spec-db` databases 

- a local installation of Postgres (either bare metal or in a Docker container)
- pgadmin (if you feel brave, you could use the CLI and psql)

#### Assumptions 
- existing astrowise schema: `astrowise`
- new astrowise data was manually added in schema `astrowise_20240911`



### safety line: use a local copy
To avoid accidents in the production database, it is wise to do all the work in a local copy of the databases.
And then when all the work is done and tested, import the results back into the production database.

I have used the **pgadmin** GUI tool for all the work.
Not only because that is much easier than using the CLI and pqsl, 
but also because it allows to directy import/export data to/from my local machine.
(Available disk space on `sdc-db.astron.nl` is a bottleneck).

#### postgres on sdc-db.astron.nl (production)
- navigate to database ``ldvadmin`` as user `ldvadmin` on `sdc-db.astron.nl` (through the ssh tunnel)
- navigate to schema `astrowise`
  - backup -> `ldvadmin_astrowise_binary.sql`
- navigate to schema `astrowise_20240911`
  - backup -> `ldvadmin_astrowise_binary_20240911.sql`

#### local postgres
- create user `ldvadmin`
- create database `ldvadmin` as user `ldvadmin`
- navigate to database `ldvadmin`
  - restore -> `ldvadmin_astrowise_binary.sql`
  - backup -> `ldvadmin_astrowise_binary_20240911.sql`

  
## STEP 1 - Combine ASTROWISE data 
- database: `ldvadmin`
- user: `ldvadmin`

If you work in a local copy, then you could now execute the following script by navigating to the `ldvadmin` database, open the 'query tool' and copy/paste the script.

If you don't work in a local copy, then it is advisable to first use the backup/rollback procedure is described in the top of the script.

After execution, the 3 tables in the `astrowise` schema should be updated with the additional data from the 3 tables in the `astrowise_20240911` schema. 

```SQL
-- combine_astrowise_tables.sql (by Nico Vermaas, oct 2024)
 
-- assumptions:
-- this script is run on a postgres server where the ldv-spec-db and ldvadmin databases are available
 
-- BACKUP existing schema (manual)
-- (1) use the pgadmin backup function to save the 'astrowise' schema to 'astrowise.sql'
-- (2) rename schema 'astrowise' to 'astrowise_backup'
--     ALTER SCHEMA astrowise RENAME TO astrowise_backup;
-- (3) create schema 'astrowise' for user 'ldvadmin'
--     CREATE SCHEMA IF NOT EXISTS astrowise;
-- (4) select schema 'astrowise' and use the restore function to load 'astrowise.sql' 
 
-- ROLL BACK to the original situation:
-- (1) remove schema 'astrowise'
--     DROP SCHEMA astrowise
-- (2) rename schema 'astrowise_backup' to 'astrowise'
--     ALTER SCHEMA astrowise_backup RENAME TO astrowise;
 
-- combine the records from the existing and new schema
 
-- pipeline (PL) dataproducts
 
-- cast some weird datatypes into TEXT to prevent cast errors
ALTER TABLE astrowise.pl_dataproducts
ALTER COLUMN activity SET DATA TYPE TEXT,
ALTER COLUMN location SET DATA TYPE TEXT,
ALTER COLUMN dp_type SET DATA TYPE TEXT;
 
ALTER TABLE astrowise_20240911.pl_dataproducts
ALTER COLUMN activity SET DATA TYPE TEXT,
ALTER COLUMN location SET DATA TYPE TEXT,
ALTER COLUMN dp_type SET DATA TYPE TEXT;
 
 
INSERT INTO astrowise.pl_dataproducts (
	activity,project,location,obsid,obsid_source,process_start,process_duration,dp_type,dp_start,dp_duration,dysco,uri,SIZE)
SELECT 
activity,
project,
location,
obsid,
obsid_source,
process_start,
process_duration,
dp_type,
dp_start,
dp_duration,
dysco,
uri,
SIZE
FROM astrowise_20240911.pl_dataproducts;
 
-- RAW dataproducts
 
-- cast some weird datatypes into TEXT to prevent cast errors
ALTER TABLE astrowise.raw_dataproducts
ALTER COLUMN activity SET DATA TYPE TEXT,
ALTER COLUMN location SET DATA TYPE TEXT,
ALTER COLUMN dp_type SET DATA TYPE TEXT;
 
ALTER TABLE astrowise_20240911.raw_dataproducts
ALTER COLUMN activity SET DATA TYPE TEXT,
ALTER COLUMN location SET DATA TYPE TEXT,
ALTER COLUMN dp_type SET DATA TYPE TEXT;
 
-- combine the records from the existing and new schema
INSERT INTO astrowise.raw_dataproducts (
	activity,project,location,
	obsid,obsid_source,
	antenna_set,instrument_filter,
	process_start,process_duration,
	core_stations,remote_stations,international_stations,
	dp_type,dp_start,dp_duration,
	dysco,uri,SIZE)
SELECT 
activity,project,location,
obsid,obsid_source,
antenna_set,instrument_filter,
process_start,process_duration,
core_stations,remote_stations,international_stations,
dp_type,dp_start,dp_duration,
dysco,uri,SIZE
FROM astrowise_20240911.raw_dataproducts;
 
 
-- unspecified dataproducts
 
-- cast some weird datatypes into TEXT to prevent cast errors
ALTER TABLE astrowise.unsp_dataproducts
ALTER COLUMN activity SET DATA TYPE TEXT,
ALTER COLUMN location SET DATA TYPE TEXT,
ALTER COLUMN dp_type SET DATA TYPE TEXT;
 
ALTER TABLE astrowise_20240911.unsp_dataproducts
ALTER COLUMN activity SET DATA TYPE TEXT,
ALTER COLUMN location SET DATA TYPE TEXT,
ALTER COLUMN dp_type SET DATA TYPE TEXT;
 
INSERT INTO astrowise.unsp_dataproducts (
	activity,project,location,obsid,obsid_source,process_start,process_duration,dp_type,dp_start,dp_duration,dysco,uri,SIZE)
SELECT 
activity,
project,
location,
obsid,
obsid_source,
process_start,
process_duration,
dp_type,
dp_start,
dp_duration,
dysco,
uri,
SIZE
FROM astrowise_20240911.unsp_dataproducts;
```

### STEP 2 - Update lofardata_dataproduct table
- database: `ldvadmin`
- user: `ldvadmin`

This query prepares the combined `lofardata_dataproduct` table in a format that can be imported later into the `ldv-spec-db` database.

#### export the result
After the query is executed (and this could take an hour) export the result. 

In pgadmin:
- navigate to `ldvadmin.lofardata_dataproduct`
  - backup -> `lofardata_dataproduct_8okt2024_binary.sql`


```SQL
-- update_ldvspec_database.sql (by Nico Vermaas, oct 2024)
-- based on documenation: https://git.astron.nl/astron-sdc/ldv-specification/-/blob/main/MIGRATION.md?ref_type=heads
 
-- assumptions:
-- this script is run on a postgres server where the ldvadmin database is available. Have public schema selected.
 
-- Create combined table
 
-- for safety reasons, do the DROP command manually first... in the correct database (ldvadmin, not ldv-spec-db).
-- DROP TABLE IF EXISTS public.lofardata_dataproduct;
 
CREATE TABLE lofardata_dataproduct(
obs_id VARCHAR(15),
oid_source VARCHAR(15),
dataproduct_source VARCHAR(20),
dataproduct_type VARCHAR(50),
project VARCHAR(50),
location VARCHAR(50),
activity VARCHAR(50),
surl VARCHAR(200),
filesize BIGINT,
antenna_set VARCHAR(50),
instrument_filter VARCHAR(50),
dysco_compression BOOLEAN);
 
INSERT INTO lofardata_dataproduct
SELECT obsid AS obs_id, obsid_source AS oid_source, 'LOFAR LTA', dp_type AS dataproduct_type, project, location, activity, uri AS surl, SIZE AS filesize, antenna_set, instrument_filter, dysco AS dysco_compression
FROM astrowise.raw_dataproducts;
 
INSERT INTO lofardata_dataproduct
SELECT obsid AS obs_id, obsid_source AS oid_source, 'LOFAR LTA', dp_type AS dataproduct_type, project, location, activity, uri AS surl, SIZE AS filesize, NULL, NULL, dysco AS dysco_compression
FROM astrowise.pl_dataproducts;
 
INSERT INTO lofardata_dataproduct
SELECT obsid AS obs_id, obsid_source AS oid_source, 'LOFAR LTA', dp_type AS dataproduct_type, project, location, activity, uri AS surl, SIZE AS filesize, dysco AS dysco_compression
FROM astrowise.unsp_dataproducts;
 
-- Remove duplicates
-- Since the surl index in our model is unique but the combined table, by above commands, does not account for this, 
-- the duplicates need to be removed after filling the table with data. 
-- For that a primary key, here 'id', needs to be constructed first (NOTE: once again, 
-- these commands may take some time due to table sizes).
 
-- Create unique id:
ALTER TABLE lofardata_dataproduct ADD COLUMN id bigserial PRIMARY KEY;
 
-- Delete duplicate surls (this query can take about an hour):
DELETE FROM lofardata_dataproduct x USING lofardata_dataproduct y WHERE x.id < y.id AND x.surl = y.surl;
```

### STEP 3 - Import results into ldv-spec-db
- database: `ldv-spec-db`
- user: `ldv_spec`

You now have the `lofardata_dataproduct_8okt2024_binary.sql` available.

It is advisable to first test the update on a local ldvspec development environment or on the sdc-dev test/acceptance system.
And then prepare a specification with one of the new SAS_ID's. 
See the main ldvspec documenation about how to set that up.

The procedure for updating the dataproducts is the same for each instance.
Importing the data can take one or multiple hours.
(it took 1 hour at ASTRON for production, 3 hours remotely for test)

In pgadmin:

- navigate to `ldv-spec-db.lofardata_dataproduct`
  - backup -> ldvspec_lofardata_dataproduct_backup_binary.sql (just in case)
  - truncate
  - restore -> `lofardata_dataproduct_8okt2024_binary.sql`

Now ldvspec has the updated dataproducts and users can specify tasks.



### STEP 4 - Update ldvadmin
- database: `ldvadmin`
- user: `ldvadmin`

If you have worked in a local copy, then you need to update the production `ldvadmin` database also, **otherwise this update will be lost** when you do the next update.


#### local postgres
- navigate to database `ldvadmin` and schema `astrowise`
  - backup -> `ldvadmin_astrowise_combined_binary.sql`

#### postgres on sdc-db.astron.nl
- go to the (production) database ``ldvadmin`` as user `ldvadmin` on `sdc-db.astron.nl` (through the ssh tunnel)
- navigate to schema `astrowise`
  - truncate
  - restore -> `ldvadmin_astrowise_combined_binary.sql`




