#!/bin/bash
set -e


function run_migrate_and_wait {
  echo -n "Waiting for database to be up and running"
  while ! python manage.py migrate --settings $1
  do
    echo -n .
    sleep 1
  done
  echo
}
set -e

run_migrate_and_wait ldvspec.settings.docker_sdc

# Ignore failing creation of the super user
python manage.py createsuperuser --settings ldvspec.settings.docker_sdc --noinput --username admin --email admin@example.com || true
export DJANGO_SETTINGS_MODULE=ldvspec.settings.docker_sdc
gunicorn ldvspec.wsgi_docker_sdc:application --bind 0.0.0.0:8000 --workers 4
