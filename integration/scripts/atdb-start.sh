#!/bin/bash
set -e


function run_migrate_and_wait {
  echo -n "Waiting for database to be up and running"
  while ! python manage.py migrate --settings $1
  do
    echo -n .
    sleep 1
  done
  echo
}
set -e

run_migrate_and_wait atdb.settings.docker_sdc

# Ignore failing creation of the super user
python manage.py createsuperuser --noinput --username admin --email admin@example.com --settings atdb.settings.docker_sdc || true
export DJANGO_SETTINGS_MODULE=atdb.settings.docker_sdc
# Should always be the last command
gunicorn atdb.wsgi_docker_sdc:application --bind 0.0.0.0:8000 --workers 4
