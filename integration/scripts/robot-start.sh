#!/bin/bash
set -e

#Wait for services to be available
echo -n "Waiting for atdb to start"
while ! curl -fs http://atdb.docker:8000/atdb/ > /dev/null
do
  echo -n .
  sleep 1
done
echo

echo -n "Waiting for ldv spec to be available"
while ! curl -fs http://ldv-specification-backend:8000/ldvspec/ > /dev/null
do
  echo -n .
  sleep 1
done
echo


robot --outputdir integration_test_result /workdir/integration-test.robot
