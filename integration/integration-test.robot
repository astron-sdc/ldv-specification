*** Settings ***
Library    OperatingSystem
Library    SeleniumLibrary
Resource    resource/keywords.robot
Suite Setup    Setup ATDB and LDVSPEC

# Keyword used for suite setup, local to this test suite
*** Keywords ***
Setup ATDB and LDVSPEC
    Login to ATDB Admin Interface
    Create Example Workflow in ATDB
    ${token}=    Create Token
    Close Browser

    Login to LDV Spec Admin Interface
    Add ATDB Processing Site    ${token}

    Add LDV Data Product Filter    obs_id

    Add Data Product    1    srm://lofar-srm.fz-juelich.de:8443/pnfs/fz-juelich.de/data/lofar/ops/projects/idols/1/L876420_SAP000_SB019_uv.MS_5840d22a.tar
    Add Data Product    5    srm://lofar-srm.fz-juelich.de:8443/pnfs/fz-juelich.de/data/lofar/ops/projects/idols/5/L876420_SAP000_SB019_uv.MS_5840d22b.tar
    Add Data Product    13   srm://lofar-srm.fz-juelich.de:8443/pnfs/fz-juelich.de/data/lofar/ops/projects/idols/13/L876420_SAP000_SB019_uv.MS_5840d22c.tar
    Add Data Product    13   srm://lofar-srm.fz-juelich.de:8443/pnfs/fz-juelich.de/data/lofar/ops/projects/idols/13/L876420_SAP000_SB019_uv.MS_1234567b.tar
    Add Data Product    21   srm://lofar-srm.fz-juelich.de:8443/pnfs/fz-juelich.de/data/lofar/ops/projects/idols/13/L876420_SAP000_SB019_uv.MS_1234567d.tar

*** Test Cases ***

Test Data Product Api Endpoint Authentication
    [Tags]      Dataproduct
    Logout from LDV Spec
    Go To     ${LDVSPEC_API}v1/data/
    Page Should Contain    credentials were not provided

Test create and submit a work specification
    [Tags]      Workspecification
    Login to LDV Spec Admin Interface

    ${id}=    Add Work Specification    5
    Submit Work Specification    ${id}

    BuiltIn.Sleep    5s

    ${count}=   Get Tasks Count In ATDB    ldv-spec:${id}
    Should Be Equal As Integers    ${count}     1

Test create and submit a work specification with a task filter
    [Tags]      Workspecification
    Login to LDV Spec Admin Interface

    ${id}=    Add Work Specification    5    my_filter
    Submit Work Specification    ${id}

    BuiltIn.Sleep    5s

    ${count}=   Get Tasks Count In ATDB    my_filter
    Should Be Equal As Integers    ${count}     1

Test create work specification with auto-submit and 2 batches
    [Tags]      Workspecification
    Login to LDV Spec Admin Interface

    ${id}=    Add Work Specification    13
    Alter Work Specification Batches And Auto Submit    ${id}

    BuiltIn.Sleep    5s

    ${count}=   Get Tasks Count In ATDB    ldv-spec:${id}
    Should Be Equal As Integers    ${count}     2

Test auto refresh
    [Tags]      Workspecification   
    Login to LDV Spec Admin Interface
    
    ${id}=    Add Work Specification    13
    Go To     ${LDVSPEC}
    
    Click Element    //*[@data-test-id="submit-to-atdb-${id}"]
    
    BuiltIn.Sleep    10s

    ${submission_status}=    Get Text    //*[@id="specification-${id}"]//*[@data-test-id="submission-status"]
    
    Should Be Equal As Strings    ${submission_status}    Submitted

Test create a group with a few work specifications
    [Tags]      Workspecification
    Login to LDV Spec Admin Interface
    Add Group   Beatles    1, 22, 13    3
    Check if Group Exists    Beatles

Test append work specifications to an existing group
    [Tags]      Workspecification
    Add to Existing Group   Beatles    21    6
    ${count}=    Count specifications in Group    Beatles
    Should Be Equal As Integers    ${count}    4

Test task submission status ready with files
    [Tags]      Workspecification
    ${specification_id}=    Add Work Specification    5
    ${submission_status}=    Get Task Submission Status    ${specification_id}
    Should Be Equal As Strings    ${submission_status}    Ready

Test task submission status ready without files
    [Tags]      Workspecification
    ${specification_id}=    Add Work Specification    9999
    ${submission_status}=    Get Task Submission Status    ${specification_id}
    Should Be Equal As Strings    ${submission_status}    Ready*

Test task submission status submitted
    [Tags]      Workspecification
    ${specification_id}=    Add Work Specification    5
    Submit Work Specification    ${specification_id}
    ${submission_status}=    Get Task Submission Status    ${specification_id}
    Should Be Equal As Strings    ${submission_status}    Submitted

# This test depends on one ATDB site added in the suite setup
Test automatically select first processing site dropdown in workspecification create form
    [Tags]      Workspecification
    Go To   ${LDVSPEC}specification/add/
    ${selected_value}=    Get Selected List Value    //*[@data-test-id="processing_site"]
    Should Be Equal As Strings    ${selected_value}    ${ATDB_SITE_NAME}

# This test depends on one ATDB site added in the suite setup
Test automatically select first processing site dropdown in group create form
    [Tags]      Workspecification
    Go To   ${LDVSPEC}group/add/
    ${selected_value}=    Get Selected List Value    //*[@data-test-id="processing_site"]
    Should Be Equal As Strings    ${selected_value}    ${ATDB_SITE_NAME}