#!/bin/bash
set -e

docker compose -f docker-compose-integration-local.yml down && docker compose -f docker-compose-integration-local.yml up robot_run --build ldv-specification-backend