FROM python:3.10
ENV PYTHONUNBUFFERED 1
# Main dependencies
RUN apt-get update && apt-get install -y postgresql-client build-essential

WORKDIR /src
COPY ldvspec/requirements /src/requirements

# install dependencies and clean up apt build libs
RUN pip install --no-cache-dir -r requirements/prod.txt

# Copy the rest of the files
COPY ldvspec .

# collect the static files and make sure that the latest database migrations are done
RUN date +"(%d %b %Y - %H:%M)" >> VERSION
RUN python manage.py collectstatic --settings=ldvspec.settings.dev --noinput

# Add expose as documentation
EXPOSE 8000

# run gunicorn
CMD exec gunicorn ldvspec.wsgi_docker_sdc:application --bind 0.0.0.0:8000 --workers 4 --worker-class gevent --timeout 90