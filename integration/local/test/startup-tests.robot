*** Settings ***
Library    OperatingSystem
Library    SeleniumLibrary
Resource   ../../resource/keywords.robot
Resource   ../../resource/variables.robot

*** Test Cases ***

LDV Specification should be available
    [Tags]      Startup
    Open Browser    ${LDVSPEC}    ${BROWSER}
    Title Should Be    LDV Specification
    [Teardown]    Close Browser

ATDB should be Available
    [Tags]      Startup
    Open Browser    ${ATDB}    ${BROWSER}
    Title Should Be    ATDB-LDV
    [Teardown]    Close Browser

Test logging into LDV Specification admin interface
    [Tags]      Startup
    Login to LDV Spec Admin Interface
    Title Should Be    Site administration | Django site admin
    [Teardown]    Close Browser

Test logging into ATDB admin interface
    [Tags]      Startup
    Login to ATDB Admin Interface
    Title Should Be    Site administration | Django site admin
    [Teardown]    Close Browser