*** Settings ***
Library    OperatingSystem
Library    SeleniumLibrary
Resource   ../../resource/keywords.robot

*** Test Cases ***

Create Example Workflow in ATDB
    [Tags]      Keyword
    Login to ATDB Admin Interface
    Create Example Workflow in ATDB
    [Teardown]    Close Browser

Add ATDB Processing Site with Create Token
    [Tags]      Keyword
    Login to ATDB Admin Interface
    ${token}=    Create Token

    Login to LDV Spec Admin Interface
    Add ATDB Processing Site    ${token}

    [Teardown]    Close Browser

Add Data Product
    [Tags]      Keyword
    [Documentation]     ***Description***
    ...     For every run of the test, the surl should be unique.
    ...     This is by design since the database has a unique constraint on surls.
    Login to LDV Spec Admin Interface
    Add Data Product    1    srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/ops/projects/lc4_030/402998/L402998_summaryIS_f5baa34a.tar
    [Teardown]    Close Browser

Add LDV Data Product Filter
    [Tags]      Keyword
    [Documentation]     ***Description***
    ...     For every run of the test, the field should be unique.
    ...     Otherwise, it might fail due to form constraints
    Login to LDV Spec Admin Interface
    Add LDV Data Product Filter    obs_id
    [Teardown]    Close Browser

Add Work Specification
    [Tags]      Keyword
    [Documentation]     ***Description***
    ...     Creating a work specification depends on the following elements to be present:
    ...     atdb processing site link
    ...     atdb workflow
    ...     a dataproduct with with 'obs_id=1' and 'surl=srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/ops/projects/lc4_030/402998/L402998_summaryIS_f5baa34a.tar'
    ...     a dataproduct filter for 'obs_id'
    ...     It returns the specification's ID
    Login to LDV Spec Admin Interface
    ${id}=  Add Work Specification    1
    [Teardown]    Close Browser

Submit Work Specification
    [Tags]      Keyword
    [Documentation]     ***Description***
    ...     Submitting a work specification depends on the following element to be present:
    ...     a work specification with ID 1
    ...     It then checks if the status has changed in LDV specification
    Login to LDV Spec Admin Interface
    Submit Work Specification    1
    [Teardown]    Close Browser

Alter Work Specification Batches And Auto Submit
    [Tags]      Keyword
    [Documentation]     ***Description***
    ...     Altering work specification depends on a non-submitted work specification to be present.
    ...     To ensure this, also a work specification is created in this test.
    ...     The rules w.r.t. adding a work specification thus also apply here
    Login to LDV Spec Admin Interface
    ${id}=  Add Work Specification    1
    Alter Work Specification Batches And Auto Submit    ${id}
    [Teardown]    Close Browser

Get Tasks Count in ATDB Should Equal One
    [Tags]      Keyword
    [Documentation]     ***Description***
    ...     Prerequisite: submission of work specification with ID 1 succeeded.
    ...     Note that checking for equality should specify the type (integers, numbers, strings).
    Login To ATDB Admin Interface
    ${count}=   Get Tasks Count In ATDB    ldv-spec:1
    Should Be Equal As Integers    ${count}     1
    [Teardown]    Close Browser


Add Group
    [Tags]      Keyword
    Login to LDV Spec Admin Interface
    Add Group   Stones    1, 2, 3    5
    [Teardown]    Close Browser