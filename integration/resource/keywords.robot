*** Settings ***
Library    OperatingSystem
Library    SeleniumLibrary
Resource   variables.robot

*** Keywords ***

# Ldvspec

Add Group
    [Arguments]    ${name}    ${obs_ids}    ${files_per_task}
    Go To   ${LDVSPEC}group/add/

    Input Text    //*[@data-test-id="group_name"]    ${name}

    Select From List By Value     //*[@data-test-id="processing_site"]    ${ATDB_SITE_NAME}
    Wait Until Element Contains    //*[@data-test-id="workflow_tag"]    environment    timeout=5s
    Select From List By Value    //*[@data-test-id="workflow_tag"]    environment
    Wait Until Element Contains    //*[@data-test-id="workflow"]    https://example.com/workflow-1    timeout=5s
    Select From List By Value    //*[@data-test-id="workflow"]    https://example.com/workflow-1

    Input Text    //*[@data-test-id="obs_ids"]    ${obs_ids}

    Input Text    //*[@data-test-id="batch_size"]    ${files_per_task}

    Click Button    //*[@data-test-id="create-update"]

Add to Existing Group
    [Arguments]    ${name}    ${obs_ids}    ${files_per_task}

    Click Element    //*[@data-test-id="edit-group-${name}"]
    Input Text    //*[@data-test-id="obs_ids"]    ${obs_ids}
    Input Text    //*[@data-test-id="batch_size"]    ${files_per_task}
    Click Button    //*[@data-test-id="create-update"]

Count specifications in Group
    [Arguments]    ${name}
    ${count}=    Get Element Count    (//div[@data-test-id="table-header-${name}"]/..//a[@data-test-id="specification-id"])
    RETURN    ${count}

Check if Group Exists
    [Arguments]    ${name}
    Go To   ${LDVSPEC}
    Page Should Contain Element    //*[@data-test-id="table-header-${name}"]

Submit Work Specification
    [Arguments]    ${specification_id}
    Go To     ${LDVSPEC}
    Click Element    //*[@data-test-id="submit-to-atdb-${specification_id}"]
    Go To    ${LDVSPEC}specification/${specification_id}/
    Element Should Not Contain    //*[@data-test-id="submission-status"]    Error
    Wait Until Element Contains    //*[@data-test-id="submission-status"]    Submitted    timeout=60s

Add Work Specification
    [Arguments]    ${obs_id}    ${task_filter}=${EMPTY}
    Go To   ${LDVSPEC}specification/add/
    Click Element    //*[@data-test-id="processing_site"]
    Select From List By Value     //*[@data-test-id="processing_site"]    ${ATDB_SITE_NAME}
    Wait Until Element Contains    //*[@data-test-id="workflow_tag"]    environment    timeout=5s
    Select From List By Value    //*[@data-test-id="workflow_tag"]    environment
    Wait Until Element Contains    //*[@data-test-id="workflow"]    https://example.com/workflow-1    timeout=5s
    Select From List By Value    //*[@data-test-id="workflow"]    https://example.com/workflow-1
    Input Text    //*[@data-test-id="obs_id"]    ${obs_id}
    Input Text    //*[@data-test-id="task-filter-input"]    ${task_filter}
    Click Button    //*[@data-test-id="create-update"]
    ${id}=    Get Text   (//div[@data-test-id="table-header-ungrouped"]/..//a[@data-test-id="specification-id"])[last()]
    RETURN    ${id}

Alter Work Specification Batches And Auto Submit
    [Arguments]    ${specification_id}
    Go To   ${LDVSPEC}specification/update/${specification_id}/
    Select Checkbox    //*[@data-test-id="auto-submit-checkbox"]
    Checkbox Should Be Selected     //*[@data-test-id="auto-submit-checkbox"]
    Input Text    //*[@data-test-id="batch-size-input"]   2
    Click Button    //*[@data-test-id="create-update"]
    Go To    ${LDVSPEC}specification/${specification_id}/
    Element Should Contain    //*[@data-test-id="batch-size"]   2
    Wait Until Element Contains    //*[@data-test-id="submission-status"]    Submitted    timeout=60s

Add Data Product
    [Arguments]    ${obs_id}    ${uniqueSurl}
    Go To    ${LDVSPEC}admin/lofardata/dataproduct/add/
    Input Text    id_obs_id    ${obs_id}
    Input Text    id_oid_source    sas
    Input Text    id_dataproduct_source    lofar
    Input Text    id_dataproduct_type    raw
    Input Text    id_project    LC01_01
    Input Text    id_location    Poznan
    Input Text    id_activity    raw observation
    Input Text    id_surl    ${uniqueSurl}
    Input Text    id_filesize    42
    Input Text    id_antenna_set    hba
    Input Text    id_instrument_filter    10MHz
    Click Element    id_dysco_compression
    Click Button    _save
    Page Should Contain Element    class:success

Add LDV Data Product Filter
    [Arguments]    ${uniqueField}
    Go To    ${LDVSPEC}admin/lofardata/dataproductfilter/add/
    Input Text    id_field    ${uniqueField}
    Input Text    id_name    ${uniqueField}
    Input Text    id_lookup_type    exact
    Click Button    _save
    Page Should Contain Element    class:success

Add ATDB Processing Site
    [Arguments]    ${token}
    Go To    ${LDVSPEC}admin/lofardata/atdbprocessingsite/add/
    Input Text    id_name    ${ATDB_SITE_NAME}
    Input Text    id_url    ${ATDB}
    Input Text    id_access_token    ${token}
    Click Button    _save
    Page Should Contain Element    class:success

Login to LDV Spec Admin Interface
    Open Browser to Django Admin Login Interface    ${LDVSPEC}admin/
    Input Django Admin Username    admin
    Input Django Admin Password    admin
    Submit credentials

Logout from LDV Spec
    Go To     ${LDVSPEC}accounts/logout/
    Click Element    //button[contains(text(), 'Log out')]

Get Task Submission Status
    [Arguments]    ${specification_id}
    Go To   ${LDVSPEC}specification/${specification_id}
    ${submission_status}=    Get Text    //*[@data-test-id="submission-status"]
    RETURN    ${submission_status}

# ATDB

Create Example Workflow in ATDB
    Go To    ${ATDB}admin/taskdatabase/workflow/add/
    Input Text    id_description   someworkflow
    Input Text    id_tag    environment
    Input Text    id_workflow_uri    https://example.com/workflow-1/
    Input Text    id_repository    https://example.com/workflow-1/
    Input Text    id_commit_id    example-tag
    Input Text    id_path    some/script.sh
    Input Text    id_oi_size_fraction    1
    Click Button    _save
    Page Should Contain Element    class:alert-success

Create Token
    Go To    ${ATDB}admin/authtoken/tokenproxy/add/
    Select From List By Value    id_user    1
    Click Button    _save
    ${token}=    Get Text    css:.alert-success a
    RETURN    ${token}

Login to ATDB Admin Interface
    Open Browser to Django Admin Login Interface    ${ATDB}admin/
    Input Django Admin Username    admin
    Input Django Admin Password    admin
    Submit credentials

Get Tasks Count in ATDB
    [Arguments]    ${element}
    Open Browser to ATDB landing page
    ${count}=   Get Element Count     //*[text()[contains(.,'${element}')]]
    RETURN  ${count}


# Shared

Open Browser to ATDB landing page
    Open Browser    ${ATDB}    ${BROWSER}    service_log_path=${{os.path.devnull}}

Open Browser to Django Admin Login Interface
    [Arguments]    ${admin_url}
    Open Browser    ${admin_url}    ${BROWSER}    service_log_path=${{os.path.devnull}}
    Title Should Be    Log in | Django site admin

Input Django Admin Username
    [Arguments]    ${username}
    Input Text    id_username    ${username}

Input Django Admin Password
    [Arguments]    ${password}
    Input Text    id_password    ${password}

Submit credentials
    Click Button    Log in