from functools import wraps
from django.conf import settings

def silk_profiler(name: str):
    """Profile a function with silk.

    Args:
        name (str): The name of the profile.
    """
    def outer(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            if settings.ENABLE_PROFILING:
                from silk.profiling.profiler import silk_profile

                with silk_profile(name=name):
                    return f(*args, **kwargs)
            else:
                return f(*args, **kwargs)

        return wrapper

    return outer