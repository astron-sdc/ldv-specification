from django.core.management import BaseCommand

from lofardata.view_helpers.dataproductinfo import prefill_distinct_dataproduct_field_cache

class Command(BaseCommand):
    help = 'Prefills the cache with data'

    def handle(self, *args, **options):
        prefill_distinct_dataproduct_field_cache()