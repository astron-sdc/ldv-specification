from django.urls import include, path
from django.conf import settings
from rest_framework.routers import DefaultRouter
from rest_framework.schemas import get_schema_view

from .views import api, DataProductView, DataProductDetailsView, InsertMultiDataproductView, GroupCreateUpdateView, \
    ATDBProcessingSiteView, WorkSpecificationCreateUpdateView, WorkSpecificationDetailView, \
    WorkSpecificationDeleteView, WorkSpecificationInputsView, WorkSpecificationATDBTasksView, \
    WorkSpecificationDatasetSizeInfoView, DataProductViewPerSasID, Overview, WorkSpecificationViewset

router = DefaultRouter()
router.register(r'workspecification', WorkSpecificationViewset, basename="workspecification")
router.register(r'processing_site', ATDBProcessingSiteView, basename='processingsite')

urlpatterns = [
    # Authentication
    path('accounts/', include('astronauth.urls')),

    # REST API
    path('api/v1/data/', DataProductView.as_view(), name='dataproduct'),
    path('api/v1/insert_dataproduct/', InsertMultiDataproductView.as_view(), name='dataproduct-insert'),
    path('api/v1/data/<int:pk>/', DataProductDetailsView.as_view(), name='dataproduct-detail-view-api'),

    path('api/v1/uws/', include('uws.urls')),
    path('api/v1/openapi/', get_schema_view(
        title="LDV Specification",
        description="API description",
        version="0.0.1"
    ), name='openapi-schema'),
    path('api/v1/', include(router.urls)),

    # GUI
    path('', Overview.as_view(), name='index'),
    path('api/', api, name='api'),
    path('specification/<int:pk>/', WorkSpecificationDetailView.as_view(), name='specification-detail'),
    path('specification/add/', WorkSpecificationCreateUpdateView.as_view(), name='specification-create'),
    path('specification/update/<int:pk>/', WorkSpecificationCreateUpdateView.as_view(), name='specification-update'),
    path('specification/delete/<int:pk>/', WorkSpecificationDeleteView.as_view(), name='specification-delete'),
    path('specification/inputs/<int:pk>/', WorkSpecificationInputsView.as_view(), name='specification-inputs'),
    path('specification/tasks/<int:pk>/', WorkSpecificationATDBTasksView.as_view(), name='specification-tasks'),
    path('specification/dataset-size-info/<int:pk>/', WorkSpecificationDatasetSizeInfoView.as_view(), name='dataset-size-info'),
    path('specification/dataproducts/<int:pk>', DataProductViewPerSasID.as_view(), name='specification-dataproducts'),
    path('group/add/', GroupCreateUpdateView.as_view(), name='group-create'),
    path('group/update/<int:pk>', GroupCreateUpdateView.as_view(), name='group-update'),
]

if settings.ENABLE_PROFILING:
    urlpatterns += [path('silk/', include('silk.urls', namespace='silk'))]