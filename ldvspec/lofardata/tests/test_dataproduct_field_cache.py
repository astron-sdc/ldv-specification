from unittest import mock

import django.test as dtest

from lofardata.models import DataProduct
from lofardata.view_helpers.dataproductinfo import get_distinct_dataproduct_field

test_data_products = [dict(obs_id='12345', oid_source='SAS', dataproduct_source='lofar',
                     dataproduct_type='paper',
                     project='LT10_10',
                     location='Dwingeloo',
                     activity='paintball',
                     surl='srm://lta-head.lofar.psnc.pl:4884/subfolder/a_some_nice.tar',
                     filesize=40,
                     antenna_set='HBA Dual Inner',
                     instrument_filter='110-190 MHz',
                     dysco_compression=True),

                dict(obs_id='123789', oid_source='SAS', dataproduct_source='lofar',
                     dataproduct_type='paper',
                     project='LT10_10',
                     location='Dwingeloo',
                     activity='gokarts',
                     surl='srm://lta-head.lofar.psnc.pl:4884/subfolder/some_very_nice.tar',
                     filesize=40,
                     antenna_set='HBA Dual Inner',
                     instrument_filter='110-190 MHz',
                     dysco_compression=True),

                dict(obs_id='123789', oid_source='SAS', dataproduct_source='lofar',
                     dataproduct_type='paper',
                     project='LT10_10_Classified',
                     location='Groningen',
                     activity='gokarts',
                     surl='srm://lta-head.lofar.psnc.pl:4884/subfolder/some_not_nice.tar',
                     filesize=40,
                     antenna_set='HBA Dual Inner',
                     instrument_filter='110-190 MHz',
                     dysco_compression=False),

                dict(obs_id='1337', oid_source='SAS', dataproduct_source='lofar',
                     dataproduct_type='plastic',
                     project='LT10_10_Classified',
                     location='Groningen',
                     activity='paintball',
                     surl='srm://lta-head.lofar.psnc.pl:4884/subfolder/some_not_very_nice.tar',
                     filesize=40,
                     antenna_set='HBA Dual Inner',
                     instrument_filter='110-190 MHz',
                     dysco_compression=False),

                dict(obs_id='3773', oid_source='SAS', dataproduct_source='lofar',
                     dataproduct_type='plastic',
                     project='LT10_10_Classified',
                     location='Dwingeloo',
                     activity='gokarts',
                     surl='srm://lta-head.lofar.psnc.pl:4884/subfolder/some_not_very_super_nice.tar',
                     filesize=40,
                     antenna_set=None,
                     instrument_filter='110-190 MHz',
                     dysco_compression=False)]


class TestDataProductFieldCache(dtest.TestCase):

    def setUp(self) -> None:
        for test_dataproduct in test_data_products:
            DataProduct.objects.create(**test_dataproduct)

    @mock.patch("lofardata.view_helpers.dataproductinfo.cache.get")
    @mock.patch("lofardata.view_helpers.dataproductinfo.cache.set")
    def test_return_distinct_values(self, cache_set_mock, cache_get_mock):
        cache_get_mock.return_value = None
        actual = get_distinct_dataproduct_field('activity')
        expected = ['paintball', 'gokarts']

        self.assertCountEqual(actual, expected, "Actual list of distinct dataproduct types does not contain the same elements as the expected mocked list (ignoring order)")

    @mock.patch("lofardata.view_helpers.dataproductinfo.cache.get")
    @mock.patch("lofardata.view_helpers.dataproductinfo.cache.set")
    def test_get_result_from_cache_when_cached(self, cache_set_mock, cache_get_mock):
        mocked_return_value = ["Zwolle", "Steenwijk"]
        cache_get_mock.return_value = mocked_return_value
        actual = get_distinct_dataproduct_field('location')

        self.assertCountEqual(actual, mocked_return_value, "Actual list of distinct dataproduct types does not contain the same elements as the expected mocked list (ignoring order)")

    @mock.patch("lofardata.view_helpers.dataproductinfo.cache.get")
    @mock.patch("lofardata.view_helpers.dataproductinfo.cache.set")
    def test_get_result_from_cache_with_correct_key(self, cache_set_mock, cache_get_mock):
        get_distinct_dataproduct_field('some_custom_field')
        cache_get_mock.assert_called_with("distinct_dataproduct_some_custom_field")

    @mock.patch("lofardata.view_helpers.dataproductinfo.cache.get")
    @mock.patch("lofardata.view_helpers.dataproductinfo.cache.set")
    def test_put_in_cache_with_correct_key_and_data_and_expiration_when_not_cached(self, cache_set_mock, cache_get_mock):
        cache_get_mock.return_value = None
        get_distinct_dataproduct_field('location')
        cache_set_mock.assert_called_with("distinct_dataproduct_location", ["Dwingeloo", "Groningen"], None)


