import unittest

from lofardata.view_helpers.specification import split_obs_ids_string


class TestSplitObsIds(unittest.TestCase):
    def test_split_obs_ids_separate_by_comma(self):
        actual = split_obs_ids_string('1,2,3')
        self.assertEqual(actual, ['1', '2', '3'])

    def test_split_obs_ids_trim_spaces(self):
        actual = split_obs_ids_string('4  ,  5,6      ')
        self.assertEqual(actual, ['4', '5', '6'])

    def test_split_obs_ids_trim_newlines(self):
        actual = split_obs_ids_string('4\n,5\n\n,\n6\n')
        self.assertEqual(actual, ['4', '5', '6'])

    def test_split_obs_ids_trim_tabs(self):
        actual = split_obs_ids_string('4\t,5\t\t,\t6\t')
        self.assertEqual(actual, ['4', '5', '6'])