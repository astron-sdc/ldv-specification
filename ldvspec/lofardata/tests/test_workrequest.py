import unittest.mock

import lofardata.task.tasks as tasks
import rest_framework.test as rtest
from django.contrib.auth import get_user_model
from lofardata.models import DataProduct, WorkSpecification, SUBMISSION_STATUS


# NOTE: Instead of specifying the exact addresses, you could use `reverse` from Django
# to create the addresses for you

class TestWorkSpecificationRequest(rtest.APITestCase):
    def setUp(self):
        self.user = get_user_model().objects.create_superuser('admin')
        self.client.force_authenticate(self.user)

    @unittest.mock.patch('lofardata.task.tasks.define_work_specification.delay')
    def test_insert_work_request(self, mocked_task):
        # Mocking business
        mocked_task.return_value = unittest.mock.MagicMock()
        mocked_task.return_value.id = 'asyncresultid'
        # ----------------------------------
        # Insert test data
        DataProduct.insert_dataproduct(12345, 'lta',
                                       'myniceinstrument', 'ms', 'blubby', 'location-uri', 'pipline',
                                       'srm://lofarlta/mynice_file.tar.gz', 123456, None, None, False)

        response = self.client.post('/ldvspec/api/v1/workspecification/',
                                    data={'filters': {'obs_id': 12345}}, format='json')

        self.assertEqual(201, response.status_code)
        inserted_work_specification = response.json()
        mocked_task.assert_called_with(inserted_work_specification['id'])
        self.assertEqual('asyncresultid', inserted_work_specification['async_task_result'])
        tasks.define_work_specification(inserted_work_specification['id'])
        work_spec_object = WorkSpecification.objects.get(pk=inserted_work_specification['id'])
        self.assertEqual(work_spec_object.submission_status, SUBMISSION_STATUS.READY)
        self.assertEqual({'surls': [{"size": 123456, "surl": 'srm://lofarlta/mynice_file.tar.gz'}]},
                         work_spec_object.inputs)

    @unittest.mock.patch('lofardata.task.tasks.define_work_specification.delay')
    def test_insert_work_request_nested_fields(self, mocked_task):
        # Mocking business
        mocked_task.return_value = unittest.mock.MagicMock()
        mocked_task.return_value.id = 'asyncresultid'
        # ----------------------------------
        # Insert test data
        DataProduct.insert_dataproduct(12345, 'lta',
                                       'myniceinstrument', 'ms', 'blubby', 'location-uri', 'pipline',
                                       'srm://lofarlta/mynice_file.tar.gz', 123456, None, None, False)
        DataProduct.insert_dataproduct(12345, 'lta',
                                       'myniceinstrument', 'ms', 'blubby', 'location-uri', 'pipline',
                                       'srm://lofarlta/mynice_file_with_rhythm.tar.gz', 123456, "HBA Dual Inner",
                                       "110-190 MHz", True)

        response = self.client.post('/ldvspec/api/v1/workspecification/',
                                    data={'filters': {'obs_id': 12345, 'dysco_compression': True}},
                                    format='json')

        self.assertEqual(201, response.status_code)
        inserted_work_specification = response.json()
        mocked_task.assert_called_with(inserted_work_specification['id'])
        self.assertEqual('asyncresultid', inserted_work_specification['async_task_result'])
        tasks.define_work_specification(inserted_work_specification['id'])
        work_spec_object = WorkSpecification.objects.get(pk=inserted_work_specification['id'])
        self.assertEqual(work_spec_object.submission_status, SUBMISSION_STATUS.READY)
        self.assertEqual({'surls': [{"size": 123456, "surl": 'srm://lofarlta/mynice_file_with_rhythm.tar.gz'}]},
                         work_spec_object.inputs)
