from unittest import mock
import django.test as dtest
from lofardata.models import DataProduct, WorkSpecification
from lofardata.tests.util import mocked_delay
from lofardata.view_helpers.dataproductinfo import (
    count_unspecified_files, retrieve_combined_information)

test_objects = [dict(obs_id='12345', oid_source='SAS', dataproduct_source='lofar',
                     dataproduct_type='observation',
                     project='LT10_10',
                     location='lta-head.lofar.psnc.pl',
                     activity='secret stuff',
                     surl='srm://lta-head.lofar.psnc.pl:4884/subfolder/a_some_nice.tar',
                     filesize=40,
                     antenna_set='HBA Dual Inner',
                     instrument_filter='110-190 MHz',
                     dysco_compression=True),

                dict(obs_id='123789', oid_source='SAS', dataproduct_source='lofar',
                     dataproduct_type='observation',
                     project='LT10_10',
                     location='lta-head.lofar.psnc.pl',
                     activity='secret stuff',
                     surl='srm://lta-head.lofar.psnc.pl:4884/subfolder/some_very_nice.tar',
                     filesize=40,
                     antenna_set='HBA Dual Inner',
                     instrument_filter='110-190 MHz',
                     dysco_compression=True),

                dict(obs_id='123789', oid_source='SAS', dataproduct_source='lofar',
                     dataproduct_type='observation',
                     project='LT10_10_Classified',
                     location='lta-head.lofar.psnc.pl',
                     activity='secret stuff',
                     surl='srm://lta-head.lofar.psnc.pl:4884/subfolder/some_not_nice.tar',
                     filesize=40,
                     antenna_set='HBA Dual Inner',
                     instrument_filter='110-190 MHz',
                     dysco_compression=False),

                dict(obs_id='1337', oid_source='SAS', dataproduct_source='lofar',
                     dataproduct_type='observation',
                     project='LT10_10_Classified',
                     location='lta-head.lofar.psnc.pl',
                     activity='secret stuff',
                     surl='srm://lta-head.lofar.psnc.pl:4884/subfolder/some_not_very_nice.tar',
                     filesize=40,
                     antenna_set='HBA Dual Inner',
                     instrument_filter='110-190 MHz',
                     dysco_compression=False),

                dict(obs_id='3773', oid_source='SAS', dataproduct_source='lofar',
                     dataproduct_type='observation',
                     project='LT10_10_Classified',
                     location='lta-head.lofar.psnc.pl',
                     activity='secret stuff',
                     surl='srm://lta-head.lofar.psnc.pl:4884/subfolder/some_not_very_super_nice.tar',
                     filesize=40,
                     antenna_set=None,
                     instrument_filter='110-190 MHz',
                     dysco_compression=False),

                dict(obs_id='0987', oid_source='SAS', dataproduct_source='lofar',
                     dataproduct_type='observation',
                     project='LT10_10_Classified',
                     location='lta-head.lofar.psnc.pl',
                     activity='secret stuff',
                     surl='srm://lta-head.lofar.psnc.pl:4884/subfolder/some_very_super_nice.tar',
                     filesize=40,
                     antenna_set='',
                     instrument_filter='',
                     dysco_compression=False),

                dict(obs_id='2626', oid_source='SAS', dataproduct_source='lofar',
                     dataproduct_type='observation',
                     project='LT10_10_Classified',
                     location='lta-head.lofar.psnc.pl',
                     activity='secret stuff',
                     surl='srm://lta-head.lofar.psnc.pl:4884/subfolder/some_beautiful.tar',
                     filesize=40,
                     antenna_set='',
                     instrument_filter='',
                     dysco_compression=None),

                dict(obs_id='2627', oid_source='SAS', dataproduct_source='lofar',
                     dataproduct_type='observation',
                     project='LT10_10_Classified',
                     location='lta-head.lofar.psnc.pl',
                     activity='secret stuff',
                     surl='srm://lta-head.lofar.psnc.pl:4884/subfolder/some_not_beautiful.tar',
                     filesize=40,
                     antenna_set='',
                     instrument_filter='',
                     dysco_compression=None),

                dict(obs_id='2627', oid_source='SAS', dataproduct_source='apertif',
                     dataproduct_type='observation',
                     project='LT10_10_Classified',
                     location='lta-head.lofar.psnc.pl',
                     activity='secret stuff',
                     surl='srm://lta-head.lofar.psnc.pl:4884/subfolder/some_very_beautiful.tar',
                     filesize=40,
                     antenna_set='',
                     instrument_filter='',
                     dysco_compression=True),

                dict(obs_id='2627', oid_source='SAS', dataproduct_source='ska',
                     dataproduct_type='observation',
                     project='LT10_10_Classified',
                     location='lta-head.lofar.psnc.pl',
                     activity='secret stuff',
                     surl='srm://lta-head.lofar.psnc.pl:4884/subfolder/some_not_so_beautiful.tar',
                     filesize=40,
                     antenna_set='',
                     instrument_filter='',
                     dysco_compression=False)]


class RetrieveDataProductInformation(dtest.TestCase):

    def setUp(self):
        for test_object in test_objects:
            DataProduct.objects.create(**test_object)

    def test_setup_correct(self):
        number_of_dataproducts = DataProduct.objects.count()
        self.assertEqual(number_of_dataproducts, len(test_objects))

    def test_no_database_entry(self):
        actual = retrieve_combined_information('not_existent')
        self.assertDictEqual(actual, {})

    def test_single_database_entry(self):
        actual = retrieve_combined_information('12345')
        self.assertEqual(actual['dataproduct_source'], ['lofar'])
        self.assertEqual(actual['dataproduct_type'], ['observation'])
        self.assertEqual(actual['project'], ['LT10_10'])
        self.assertEqual(actual['activity'], ['secret stuff'])
        self.assertEqual(actual['antenna_set'], ['HBA Dual Inner'])
        self.assertEqual(actual['instrument_filter'], ['110-190 MHz'])

    def test_combined_database_entries(self):
        actual = retrieve_combined_information('123789')
        self.assertEqual(actual['project'], ['LT10_10', 'LT10_10_Classified'])

    def test_single_with_none_value(self):
        actual = retrieve_combined_information('3773')
        self.assertEqual(actual['antenna_set'], [])

    def test_single_with_default_values(self):
        actual = retrieve_combined_information('0987')
        self.assertEqual(actual['antenna_set'], [])

    def test_dysco_compression_true_and_false(self):
        actual = retrieve_combined_information('123789')
        self.assertEqual(actual['dysco_compression'], [0.5])

    def test_dysco_compression_true(self):
        actual = retrieve_combined_information('12345')
        self.assertEqual(actual['dysco_compression'], [1])

    def test_dysco_compression_false(self):
        actual = retrieve_combined_information('1337')
        self.assertEqual(actual['dysco_compression'], [0])

    def test_dysco_compression_only_none(self):
        actual = retrieve_combined_information('2626')
        self.assertEqual(actual['dysco_compression'], [0])

    def test_dysco_compression_partly_none(self):
        actual = retrieve_combined_information('2627')
        self.assertEqual(actual['dysco_compression'], [0.3333333333333333])


class CountUnspecifiedFiles(dtest.TestCase):

    @mock.patch(
        "lofardata.task.tasks.define_work_specification.delay", side_effect=mocked_delay
    )
    def setUp(self, mock_delay: mock.MagicMock):

        self.specification = WorkSpecification.objects.create(filters={"obs_id": test_objects[0]["obs_id"]})
        mock_delay.assert_called_once_with(self.specification.id)

        for test_object in test_objects:
            DataProduct.objects.create(**test_object)

    def test_setup_correct(self):
        number_of_dataproducts = DataProduct.objects.count()
        self.assertEqual(number_of_dataproducts, len(test_objects))

    def test_no_unspecified_files(self):
        actual = count_unspecified_files(self.specification.id)
        self.assertEqual(actual, 0, "Invalid amount of unspecified files")

    def test_some_unspecified_files(self):
        unspec_file = dict(
            obs_id=self.specification.filters["obs_id"],
            oid_source='SAS',
            dataproduct_source='ska',
            dataproduct_type='Unknown',
            project='LT10_10_Classified',
            location='lta-head.lofar.psnc.pl',
            activity='secret stuff',
            surl='srm://lta-head.lofar.psnc.pl:4884/subfolder/TheMeaningOfLifeTheUniverseAndEverything.tar',
            filesize=42,
            antenna_set='',
            instrument_filter='',
            dysco_compression=False)

        # Create the Unspecified file
        DataProduct.objects.create(**unspec_file)

        actual = count_unspecified_files(self.specification.id)
        self.assertEqual(actual, 1, "Invalid amount of unspecified files")
