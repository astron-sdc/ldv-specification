import rest_framework.test as rtest
from django.contrib.auth.models import User
from lofardata.models import DataProduct, DataProductFilter

test_object_values = [dict(obs_id='12345', oid_source='SAS', dataproduct_source='lofar',
                           dataproduct_type='observation',
                           project='LT10_10',
                           location='surfsara.nl:4884',
                           activity='observation',
                           surl='srm://surfsara.nl:4884/...',
                           filesize=40,
                           antenna_set="HBA Dual Inner",
                           instrument_filter="110-190 MHz",
                           dysco_compression=True),
                      dict(obs_id='12346', oid_source='SAS', dataproduct_source='lofar',
                           dataproduct_type='observation',
                           project='LT10_10',
                           location='surfsara.nl:4884',
                           activity='pipeline',
                           surl='srm://surfsara.nl:4884/---',
                           filesize=40,
                           antenna_set="HBA Dual Inner",
                           instrument_filter="110-190 MHz",
                           dysco_compression=True),
                      dict(obs_id='12347', oid_source='SAS', dataproduct_source='lofar',
                           dataproduct_type='observation',
                           project='LT10_10',
                           location='surfsara.nl:4884',
                           activity='observation',
                           surl='srm://surfsara.nl:4884/xxx',
                           filesize=40,
                           antenna_set=None,
                           instrument_filter=None,
                           dysco_compression=True)]


class TestDataFilter(rtest.APITestCase):
    def setUp(self):
        self.user = User.objects.create_superuser('admin')
        self.client.force_authenticate(self.user)

        for test_object in test_object_values:
            DataProduct.insert_dataproduct(**test_object)

    def get_current_query_parameters(self):
        response = self.client.get('/ldvspec/api/v1/openapi/')
        query_parameters = response.data['paths']['/ldvspec/api/v1/data/']['get']['parameters']
        return {parameter['name']: parameter for parameter in query_parameters}

    def test_add_custom_filter(self):
        self.assertNotIn('activity', self.get_current_query_parameters(), 'Test is invalid! Update')

        dp_filter = DataProductFilter(field='activity', name='activity_type', lookup_type='exact')
        dp_filter.save()

        response = self.client.get('/ldvspec/api/v1/data/?activity_type=pipeline')
        self.assertEqual(1, response.data['count'])
        self.assertEqual('pipeline', response.data['results'][0]['activity'])

        response = self.client.get('/ldvspec/api/v1/data/?activity_type=observation')
        self.assertEqual(2, response.data['count'])
        self.assertEqual('observation', response.data['results'][0]['activity'])

        self.assertIn('activity_type', self.get_current_query_parameters())


