""" Tests for propagating fields from groups to underlying work specifications """

from unittest import mock

from django import test as dtest
from django.contrib.auth.models import User
from django.urls import reverse
from lofardata.models import (
    ATDBProcessingSite,
    DataProductFilter,
    Group,
    WorkSpecification, SUBMISSION_STATUS,
)
from lofardata.tests.util import mocked_delay
from lofardata.view_helpers.specification import create_work_specifications_for_group
from rest_framework.status import HTTP_302_FOUND


class GroupUpdatePropagation(dtest.TestCase):
    @mock.patch(
        "lofardata.task.tasks.define_work_specification.delay", side_effect=mocked_delay
    )
    def setUp(self, _):
        # Create the standard set of filter fields
        for field in ["obs_id", "activity", "project", "dataproduct_type", "location"]:
            DataProductFilter.objects.create(
                field=field, name="Not Important", lookup_type="exact"
            )

        self.user, _ = User.objects.get_or_create()
        self.site, _ = ATDBProcessingSite.objects.get_or_create(
            name="Example", url="https://example.com", access_token="TopSecret"
        )

        self.group, _ = Group.objects.get_or_create(
            name="SomeGroup",
            processing_site=self.site,
            selected_workflow="TestWorkflow",
            selected_workflow_tag="TestTag",
        )

        self.specs = create_work_specifications_for_group(
            group=self.group,
            created_by=self.user,
            obs_ids=["123", "456", "789"],
            batch_size=4,
        )

        for spec in self.specs:
            spec.submission_status = SUBMISSION_STATUS.READY
            spec.save()

    @mock.patch(
        "lofardata.task.tasks.define_work_specification.delay", side_effect=mocked_delay
    )
    def test_group_workflow_propagation(self, _):

        data = {
            "name": "Example",
            "processing_site": self.site.name,
            "selected_workflow_tag": "NewTestTag",
            "selected_workflow": "NewWorkflow",
            "action": "Create",
        }

        self.client.force_login(self.user)
        res = self.client.post(
            reverse("group-update", kwargs={"pk": self.group.pk}), data=data
        )
        self.assertEqual(res.status_code, HTTP_302_FOUND, "Update should redirect")
        self.assertEqual(
            res.get("Location"),
            reverse("index"),
            "Update should redirect to index page",
        )

        for ws in self.specs:
            ws.refresh_from_db()
            self.assertEqual(ws.selected_workflow, data["selected_workflow"])
            self.assertEqual(ws.selected_workflow_tag, data["selected_workflow_tag"])

    @mock.patch(
        "lofardata.task.tasks.define_work_specification.delay", side_effect=mocked_delay
    )
    def test_atdb_site_propagation(self, _):
        new_site, _ = ATDBProcessingSite.objects.get_or_create(
            name="Other", url="https://example.com", access_token="TopSecret"
        )

        data = {
            "name": "Example",
            "processing_site": new_site.name,
            "selected_workflow_tag": "NewTestTag",
            "selected_workflow": "NewWorkflow",
            "action": "Create",
        }

        self.client.force_login(self.user)
        res = self.client.post(
            reverse("group-update", kwargs={"pk": self.group.pk}), data=data
        )
        self.assertEqual(res.status_code, HTTP_302_FOUND, "Update should redirect")
        self.assertEqual(
            res.get("Location"),
            reverse("index"),
            "Update should redirect to index page",
        )

        for ws in self.specs:
            ws.refresh_from_db()
            self.assertEqual(
                ws.processing_site, new_site, "Processing site not updated"
            )

    @mock.patch(
        "lofardata.task.tasks.define_work_specification.delay", side_effect=mocked_delay
    )
    def test_predecessor_propagation(self, _):
        pre_ws = WorkSpecification.objects.create()

        data = {
            "name": "Example",
            "processing_site": self.site.name,
            "selected_workflow_tag": self.group.selected_workflow_tag,
            "selected_workflow": self.group.selected_workflow,
            "action": "Create",
            "predecessor_specification": str(pre_ws.pk),
        }

        self.client.force_login(self.user)
        res = self.client.post(
            reverse("group-update", kwargs={"pk": self.group.pk}), data=data
        )
        self.assertEqual(res.status_code, HTTP_302_FOUND, "Update should redirect")
        self.assertEqual(
            res.get("Location"),
            reverse("index"),
            "Update should redirect to index page",
        )

        for ws in self.specs:
            ws.refresh_from_db()
            self.assertEqual(
                ws.predecessor_specification, pre_ws, "Predecessor is not updated"
            )

    @mock.patch(
        "lofardata.task.tasks.define_work_specification.delay", side_effect=mocked_delay
    )
    def test_predecessor_in_group_propagation(self, _):
        """Tests that the predecessor is not propagated if the Pre WS is in the Group"""
        data = {
            "name": "Example",
            "processing_site": self.site.name,
            "selected_workflow_tag": self.group.selected_workflow_tag,
            "selected_workflow": self.group.selected_workflow,
            "action": "Create",
            "predecessor_specification": str(self.specs[0].pk),
        }

        self.client.force_login(self.user)
        res = self.client.post(
            reverse("group-update", kwargs={"pk": self.group.pk}), data=data
        )
        self.assertEqual(res.status_code, HTTP_302_FOUND, "Update should redirect")
        self.assertEqual(
            res.get("Location"),
            reverse("index"),
            "Update should redirect to index page",
        )

        for idx, ws in enumerate(self.specs):
            ws.refresh_from_db()

            if idx == 0:
                self.assertIsNone(
                    ws.predecessor_specification, "Task can not be it's own predecessor"
                )
            else:
                self.assertEqual(
                    ws.predecessor_specification,
                    self.specs[0],
                    "Predecessor is not updated",
                )

    @mock.patch(
        "lofardata.task.tasks.define_work_specification.delay", side_effect=mocked_delay
    )
    def test_filter_propagation(self, _):
        self.client.force_login(self.user)
        for filter_name, filter_value in [("project", "Kerbal Space Program 3"), ("activity", "Warp9"),
                                          ("location", "Mars"), ("dataproduct_type", "SpuriousCommunication"), ("project", "Kerbal Space Program 4")]:
            data = {"name": "Example", "processing_site": self.site.name,
                    "selected_workflow_tag": self.group.selected_workflow_tag,
                    "selected_workflow": self.group.selected_workflow, "action": "Create", filter_name: filter_value}
            res = self.client.post(
                reverse("group-update", kwargs={"pk": self.group.pk}), data=data
            )
            self.assertEqual(res.status_code, HTTP_302_FOUND, "Update should redirect")
            self.assertEqual(
                res.get("Location"),
                reverse("index"),
                "Update should redirect to index page",
            )

            for ws in self.specs:
                ws.refresh_from_db()
                self.assertEqual(
                    ws.filters.get(filter_name), filter_value, f"{filter_name} filter is not updated"
                )
                # TODO: check if the define_specification is ran for this WS

        # Clear a filter that was set previously
        del data["project"]
        self.client.post(
            reverse("group-update", kwargs={"pk": self.group.pk}), data=data
        )

        for ws in self.specs:
            ws.refresh_from_db()
            self.assertEqual(
                ws.filters.get("project"), None, f"Project filter is not cleared"
            )