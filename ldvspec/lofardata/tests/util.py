"""Utility functions for testing"""

from typing import Optional

from lofardata.models import WorkSpecification, AsyncResult


def mocked_delay(*args) -> AsyncResult:
    """Mock the delay used in saving a WorkSpecification"""
    ws: WorkSpecification = WorkSpecification.objects.get(pk=args[0])
    ws.async_task_result = 42  # prevents infinite recursion in task
    ws.save()

    return AsyncResult(id="Test", backend=None)


class SessionMock():

    # Example get (not yet used)
    # def get(self, *args, **kwargs):
    #     url = args[0]
    #
    #     return MockResponse({}, 200)

    def post(self, *args, **kwargs):
        """Mocked ATDB API"""

        data = kwargs.get("json", {})

        if data.get("test_arg"):
            return MockResponse({"id": data["test_arg"]}, 201)

        if data.get("atdb_kaput"):
            return MockResponse({}, 201)

        # Tests for Empty Inputs
        if len(data.get("inputs", [])) == 0:
            return MockResponse({}, 400)

        # Successful creation of task
        return MockResponse({"id": 42}, 201)


class MockResponse:
    """Mocked requests.Response class

    Args:
        json_data (dict): dictionary containing the response
        status_code (int): HTTP Status code
        text (str): Optional str representation, useful for HTTP 50X errors

    Implemented methods/properties
        - json() -> dict
        - status_code -> int
        - ok -> boolean (set automatically; status_code < 400)
        - text -> str (optional, specifically for errors)

    Examples:

        - `MockResponse({"status": "approved"}, 200)`=
        - `MockResponse({"detail": "authentication details not provided"}, 401)`
        - `MockResponse({}, 500, "Server Error")`

    Usage with `mock.patch`:
    ```
    def mocked_get(*args, **kwargs):
        url = args[0]

        if url == "https://example.com/:
            return MockResponse({"ok": True}, 200)

        return MockResponse(None, 404)

    def mocked_post(*args, **kwargs):
        url = args[0]

        # Can check the json body
        data = kwargs.get("json")

        if "foo" in data:
            return MockResponse({"foo": "bar"}, 200)
        elif "tea" in data:
            return  MockResponse(None, 400, "Coffee")

        return MockResponse(None, 404)

    @mock.patch("requests.post", side_effect=mocked_post)
    @mock.patch("requests.get", side_effect=mocked_get)
    def test_something(self, mocked_get, mocked_post):
        # Can check the mocked_* for calls and arguments
        # tip: mock.ANY can be used to allow all arguments
        pass

    ```
    """

    def __init__(
            self, json_data: Optional[dict], status_code: int, text: Optional[str] = None
    ):
        self.json_data = json_data
        self.status_code = status_code
        self.ok = status_code < 400
        self.text = text

    def json(self) -> Optional[dict]:
        """Dictionary of response data"""
        return self.json_data
