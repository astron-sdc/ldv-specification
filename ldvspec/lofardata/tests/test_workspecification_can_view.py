import unittest

from django.contrib.auth.models import User
from lofardata.models import WorkSpecification


class WorkSpecificationCanView(unittest.TestCase):
    def test_view_by_staff(self):
        staff_user = User(pk=1, is_staff=True)
        owner = User(pk=2, is_staff=False)
        workspecification = WorkSpecification(created_by=owner)
        self.assertTrue(workspecification.can_be_viewed_by(staff_user))

    def test_view_by_owner(self):
        owner = User(pk=1, is_staff=False)
        workspecification = WorkSpecification(created_by=owner)
        self.assertTrue(workspecification.can_be_viewed_by(owner))

    def test_view_by_non_owner(self):
        owner = User(pk=1, is_staff=False)
        other_user = User(pk=2, is_staff=False)
        workspecification = WorkSpecification(created_by=owner)
        self.assertTrue(workspecification.can_be_viewed_by(other_user))