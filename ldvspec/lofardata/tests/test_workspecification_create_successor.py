import unittest

from lofardata.models import WorkSpecification, ATDBProcessingSite


class WorkSpecificationCreateSuccessor(unittest.TestCase):
    def test_create_successor_copies_properties(self):
        filters = {"first_name": "John", "last_name": "Swimlane"}
        atdb_processing_site: ATDBProcessingSite = ATDBProcessingSite(name="prod", url="astron.prod", access_token="123")
        work_specification = WorkSpecification(filters=filters, processing_site=atdb_processing_site)

        successor = work_specification.create_successor()

        self.assertEqual(successor.predecessor_specification, work_specification)
        self.assertEqual(successor.processing_site, work_specification.processing_site)
        self.assertEqual(successor.filters, work_specification.filters)

