import unittest

from django.contrib.auth.models import User
from lofardata.models import WorkSpecification


class WorkSpecificationCanEdit(unittest.TestCase):
    def test_edit_by_staff(self):
        staff_user = User(pk=1, is_staff=True)
        owner = User(pk=2, is_staff=False)
        workspecification = WorkSpecification(created_by=owner)
        workspecification._state.adding = False # existing workspecification, exists in database
        self.assertTrue(workspecification.can_be_edited_by(staff_user))

    def test_edit_by_owner(self):
        owner = User(pk=1, is_staff=False)
        workspecification = WorkSpecification(created_by=owner)
        workspecification._state.adding = False # existing workspecification, exists in database
        self.assertTrue(workspecification.can_be_edited_by(owner))

    def test_edit_by_non_owner(self):
        owner = User(pk=1, is_staff=False)
        other_user = User(pk=2, is_staff=False)
        workspecification = WorkSpecification(created_by=owner)
        workspecification._state.adding = False # existing workspecification, exists in database
        self.assertFalse(workspecification.can_be_edited_by(other_user))

    def test_edit_when_new(self):
        owner = User(pk=1, is_staff=False)
        other_user = User(pk=2, is_staff=False)
        workspecification = WorkSpecification(created_by=owner)
        workspecification._state.adding = True # newly created, not added to the database yet
        self.assertTrue(workspecification.can_be_edited_by(other_user))

