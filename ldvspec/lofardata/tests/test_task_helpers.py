import unittest
from unittest import mock
import django.test as dtest

import lofardata.task.task_helpers as helpers
from lofardata.task.custom_exceptions import RequestNotOk, InvalidLocation, InvalidPredecessor, WorkSpecificationNoSite, \
    WorkerResponseNotOk, ATDBKaput, InvalidSurl
from lofardata.task.session_store import SessionStore

from lofardata.models import WorkSpecification, PURGE_POLICY, SUBMISSION_STATUS, ATDBProcessingSite, DataProduct, \
    DataProductFilter, DataFilterInclusionType, DataFilterCardinalityType

from lofardata.tests.util import mocked_delay, SessionMock


class ParseSurls(unittest.TestCase):
    @mock.patch('lofardata.task.tasks.define_work_specification.delay', side_effect=mocked_delay)
    def setUp(self, mock_delay):
        self.work_spec = WorkSpecification.objects.create()

    def test_project_anything_then_underscore_then_digits(self):
        actual = helpers.parse_surl(
            'srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/ops/projects/lc4_030/402998/L402998_summaryIS_f5baa34a.tar', self.work_spec)
        self.assertEqual(actual['project'], 'lc4_030')

    def test_project_anything_then_digits(self):
        actual = helpers.parse_surl(
            'srm://lofar-srm.fz-juelich.de:8443/pnfs/fz-juelich.de/data/lofar/ops/projects/commissioning2012/59765/L59765_SAP002_B000_S0_P000_bf.raw_02d36e06.tar', self.work_spec)
        self.assertEqual(actual['project'], 'commissioning2012')

    def test_project_only_digits(self):
        actual = helpers.parse_surl(
            'srm://lofar-srm.fz-juelich.de:8443/pnfs/fz-juelich.de/data/lofar/ops/projects/123456/59765/L59765_SAP002_B000_S0_P000_bf.raw_02d36e06.tar', self.work_spec)
        self.assertEqual(actual['project'], '123456')

    def test_project_anything(self):
        actual = helpers.parse_surl(
            'srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/ops/projects/1@!_$52aaP/402998/L402998_summaryIS_f5baa34a.tar', self.work_spec)
        self.assertEqual(actual['project'], '1@!_$52aaP')

    def test_project_empty(self):
        with self.assertRaises(InvalidSurl):
            helpers.parse_surl(
                'srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/ops/projects//402998/L402998_summaryIS_f5baa34a.tar', self.work_spec)

    def test_no_project(self):
        with self.assertRaises(InvalidSurl):
            helpers.parse_surl(
                'srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/ops/L402998_summaryIS_f5baa34a.tar', self.work_spec)

    def test_no_project_but_something_else(self):
        with self.assertRaises(InvalidSurl):
            helpers.parse_surl('srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/software/RUG_build_2015.tar', self.work_spec)

    def test_no_valid_location(self):
        with self.assertRaises(InvalidLocation):
            helpers.parse_surl('srm://data/lofar/ops/projects/lc4_030/402998/L402998_summaryIS_f5baa34a.tar', self.work_spec)

    def test_valid_location_sara(self):
        actual = helpers.parse_surl(
            'srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/ops/projects/lc4_030/402998/L402998_summaryIS_f5baa34a.tar', self.work_spec)
        self.assertEqual(actual['location'], 'srm.grid.sara.nl')

    def test_valid_location_jeulich(self):
        actual = helpers.parse_surl(
            'srm://lofar-srm.fz-juelich.de:8443/pnfs/fz-juelich.de/data/lofar/ops/projects/lt10_002/682020/L682020_SAP000_B120_S1_P000_bf.h5_091c1e78.tar', self.work_spec)
        self.assertEqual(actual['location'], 'lofar-srm.fz-juelich.de')

    def test_valid_location_poznan(self):
        actual = helpers.parse_surl(
            'srm://lta-head.lofar.psnc.pl:8443/lofar/ops/projects/lt10_002/698739/L698739_SAP000_B045_S1_P000_bf.h5_4a2eb67a.tar', self.work_spec)
        self.assertEqual(actual['location'], 'lta-head.lofar.psnc.pl')


class SplitEntries(unittest.TestCase):
    def test_no_splitting(self):
        with self.assertRaises(RequestNotOk):
            helpers.split_entries_to_batches([*range(10)], 0)

    def test_splitting_empty_array(self):
        res = helpers.split_entries_to_batches([], 2)
        self.assertListEqual(res, [])

    def test_splitting_bs1(self):
        res = helpers.split_entries_to_batches([*range(10)], 1)
        self.assertEqual(len(res), 10)
        # TODO: check contents

    def test_splitting_exact_match(self):
        res = helpers.split_entries_to_batches([*range(9)], 3)
        self.assertEqual(len(res), 3)

    def test_splitting_with_left_over(self):
        res = helpers.split_entries_to_batches([*range(10)], 3)
        self.assertEqual(len(res), 4)
        self.assertEqual(len(res[-1]), 1)


class SendToATDB(unittest.TestCase):
    @mock.patch('lofardata.task.tasks.define_work_specification.delay', side_effect=mocked_delay)
    def setUp(self, _):
        self.atdb_site = ATDBProcessingSite(url="http://x-men.com", access_token="wolverine-37")
        self.work_spec = WorkSpecification.objects.create()

    @mock.patch.object(SessionStore, 'get_session', return_value=SessionMock())
    def test_send_to_atdb_without_response_task_ids(self, _):
        session = SessionStore.get_session()
        payload = {"atdb_kaput": True}
        self.assertRaises(ATDBKaput,
                          lambda: helpers.send_to_atdb(payload, session, self.atdb_site, self.work_spec))
        self.work_spec.refresh_from_db()
        self.assertEqual(self.work_spec.submission_status, SUBMISSION_STATUS.ERROR)
        self.assertEqual(self.work_spec.atdb_task_ids, [])

    @mock.patch.object(SessionStore, 'get_session', return_value=SessionMock())
    def test_send_to_atdb_error(self, _):
        session = SessionStore.get_session()
        payload = {"inputs": []}
        self.assertRaises(WorkerResponseNotOk,
                          lambda: helpers.send_to_atdb(payload, session, self.atdb_site, self.work_spec))
        self.work_spec.refresh_from_db()
        self.assertEqual(self.work_spec.submission_status, SUBMISSION_STATUS.ERROR)

    @mock.patch.object(SessionStore, 'get_session', return_value=SessionMock())
    def test_send_to_atdb_with_task_ids(self, mock_session):
        atdb_task_id = 37
        session = SessionStore.get_session()
        payload = {"test_arg": atdb_task_id}
        helpers.send_to_atdb(payload, session, self.atdb_site, self.work_spec)
        self.assertEqual(self.work_spec.atdb_task_ids, [atdb_task_id])

    @mock.patch.object(SessionStore, 'get_session', return_value=SessionMock())
    def test_send_to_atdb_adding_task_ids(self, mock_session):
        atdb_task_id_1 = 73
        self.work_spec.atdb_task_ids = [atdb_task_id_1]
        atdb_task_id_2 = 37
        session = SessionStore.get_session()
        payload = {"test_arg": atdb_task_id_2}
        helpers.send_to_atdb(payload, session, self.atdb_site, self.work_spec)
        self.assertEqual(self.work_spec.atdb_task_ids, [atdb_task_id_1, atdb_task_id_2])


class CreatePayload(unittest.TestCase):

    @mock.patch('lofardata.task.tasks.define_work_specification.delay', side_effect=mocked_delay)
    def setUp(self, mock_delay):
        self.work_spec = WorkSpecification.objects.create()
        self.work_spec.purge_policy = PURGE_POLICY.NO
        self.work_spec.selected_workflow = "MyCoolWorkflow"
        self.work_spec.task_filter = "filter_this!"
        self.work_spec.save()
        mock_delay.assert_called_with(self.work_spec.id)

    def test_create_payload_basic(self):
        surl = "srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/ops/projects/lc0_012/86574/L86574_SAP000_SB139_uv.MS_bc088c70.tar"
        size = 97884160
        batch = [{"size": size, "surl": surl}]

        actual = helpers.create_payload(batch, self.work_spec)
        expected = {
            "project": "lc0_012",
            "sas_id": "86574",
            "task_type": "regular",
            "filter": "filter_this!",
            "purge_policy": self.work_spec.purge_policy,
            "new_status": "defining",
            "new_workflow_uri": self.work_spec.selected_workflow,
            "size_to_process": size,
            "inputs": [{"size": size, "surl": surl, "type": "File", "location": "srm.grid.sara.nl"}],
        }
        self.assertDictEqual(actual, expected, "Invalid payload for basic testcase")

    @unittest.skip("FIXME: SDC-905")
    def test_create_payload_multiple_locations(self):
        surl_one = "srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/ops/projects/lc0_012/86574/L86574_SAP000_SB139_uv.MS_bc088c70.tar"
        size_one = 97884160
        surl_two = "srm://lta-head.lofar.psnc.pl:8443/lofar/ops/projects/lt10_002/698739/L698739_SAP000_B045_S1_P000_bf.h5_4a2eb67a.tar"
        size_two = 1234567
        batch = [{"size": size_one, "surl": surl_one}, {"size": size_two, "surl": surl_two}]

        actual = helpers.create_payload(batch, self.work_spec)
        expected = {
            "project": "lc0_012",
            "sas_id": "86574",
            "task_type": "regular",
            "filter": "filter_this!",
            "purge_policy": self.work_spec.purge_policy,
            "new_status": "defining",
            "new_workflow_uri": self.work_spec.selected_workflow,
            "size_to_process": size_one + size_two,
            "inputs": [{"size": size_one, "surl": surl_one, "type": "File", "location": "srm.grid.sara.nl"},
                       {"size": size_two, "surl": surl_two, "type": "File", "location": "lta-head.lofar.psnc.pl"}],
        }
        self.assertDictEqual(actual, expected, "Invalid payload for multiple locations testcase")

    def test_create_payload_multiple_projects(self):
        # Placeholder #FIXME: SDC-905
        pass

    def test_create_payload_multiple_sas_ids(self):
        # Placeholder #FIXME: SDC-905
        pass

    def test_create_payload_with_predecessor(self):
        surl = "srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/ops/projects/lc0_012/86574/L86574_SAP000_SB139_uv.MS_bc088c70.tar"
        size = 97884160
        batch = [{"size": size, "surl": surl}]
        predecessor = 42

        actual = helpers.create_payload(batch, self.work_spec, predecessor)
        expected = {
            "project": "lc0_012",
            "sas_id": "86574",
            "task_type": "regular",
            "filter": "filter_this!",
            "purge_policy": self.work_spec.purge_policy,
            "new_status": "defining",
            "new_workflow_uri": self.work_spec.selected_workflow,
            "size_to_process": size,
            "inputs": [{"size": size, "surl": surl, "type": "File", "location": "srm.grid.sara.nl"}],
            "predecessor": predecessor
        }
        self.assertDictEqual(actual, expected, "Invalid payload with predecessor")

    def test_create_payload_empty_batch(self):
        batch = []

        self.assertRaises(RequestNotOk, lambda: helpers.create_payload(batch, self.work_spec))

    def test_create_payload_empty_task_filter(self):
        surl = "srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/ops/projects/lc0_012/86574/L86574_SAP000_SB139_uv.MS_bc088c75.tar"
        size = 97884260
        batch = [{"size": size, "surl": surl}]

        self.work_spec.task_filter = ''
        actual_filter = helpers.create_payload(batch, self.work_spec)["filter"]
        expected_filter = f"ldv-spec:{self.work_spec.pk}"

        self.assertEqual(actual_filter, expected_filter, "Invalid payload for empty task filter testcase")

    def test_create_payload_none_task_filter(self):
        surl = "srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/ops/projects/lc0_012/86574/L86574_SAP000_SB139_uv.MS_bc088c75.tar"
        size = 97884260
        batch = [{"size": size, "surl": surl}]

        self.work_spec.task_filter = None
        actual_filter = helpers.create_payload(batch, self.work_spec)["filter"]
        expected_filter = f"ldv-spec:{self.work_spec.pk}"

        self.assertEqual(actual_filter, expected_filter, "Invalid payload for none task filter testcase")

    def test_create_payload_whitespace_task_filter(self):
        surl = "srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/ops/projects/lc0_012/86574/L86574_SAP000_SB139_uv.MS_bc088c76.tar"
        size = 97884265
        batch = [{"size": size, "surl": surl}]

        self.work_spec.task_filter = ' '
        actual_filter = helpers.create_payload(batch, self.work_spec)["filter"]
        expected_filter = f"ldv-spec:{self.work_spec.pk}"

        self.assertEqual(actual_filter, expected_filter, "Invalid payload for whitespace filter testcase")


class GetATDBPredecessorTaskId(unittest.TestCase):
    @mock.patch('lofardata.task.tasks.define_work_specification.delay', side_effect=mocked_delay)
    def test_get_atdb_predecessor_task_id_no_predecessor(self, mock_delay):
        self.work_spec = WorkSpecification.objects.create()
        self.work_spec.save()

        mock_delay.assert_called_with(self.work_spec.id)

        actual = helpers.get_atdb_predecessor_task_id(self.work_spec)
        expected = None
        self.assertEqual(actual, expected, "Retrieving task ids of not existing predecessor is not None")

    @mock.patch('lofardata.task.tasks.define_work_specification.delay', side_effect=mocked_delay)
    def test_get_atdb_predecessor_task_id_basic(self, mock_delay):
        self.predecessor = WorkSpecification.objects.create()
        self.predecessor.atdb_task_ids = [1]
        self.predecessor.save()
        mock_delay.assert_called_with(self.predecessor.id)

        self.work_spec = WorkSpecification.objects.create()
        self.work_spec.predecessor_specification = self.predecessor
        self.work_spec.save()
        mock_delay.assert_called_with(self.work_spec.id)

        actual = helpers.get_atdb_predecessor_task_id(self.work_spec)
        expected = 1
        self.assertEqual(actual, expected, "Retrieving task id of not existing predecessor is incorrect")

    @mock.patch('lofardata.task.tasks.define_work_specification.delay', side_effect=mocked_delay)
    def test_get_atdb_predecessor_task_id_missing_task_ids(self, mock_delay):
        self.predecessor = WorkSpecification.objects.create()
        self.predecessor.save()
        mock_delay.assert_called_with(self.predecessor.id)

        self.work_spec = WorkSpecification.objects.create()
        self.work_spec.predecessor_specification = self.predecessor
        self.work_spec.save()
        mock_delay.assert_called_with(self.work_spec.id)

        self.assertRaises(InvalidPredecessor, lambda: helpers.get_atdb_predecessor_task_id(self.work_spec))

    @mock.patch('lofardata.task.tasks.define_work_specification.delay', side_effect=mocked_delay)
    def test_get_atdb_predecessor_task_multiple_ids(self, mock_delay):
        self.predecessor = WorkSpecification.objects.create()
        self.predecessor.atdb_task_ids = [1, 2]
        self.predecessor.save()
        mock_delay.assert_called_with(self.predecessor.id)

        self.work_spec = WorkSpecification.objects.create()
        self.work_spec.predecessor_specification = self.predecessor
        self.work_spec.save()

        mock_delay.assert_called_with(self.work_spec.id)
        mock_delay.assert_called_with(self.work_spec.id)

        self.assertRaises(InvalidPredecessor, lambda: helpers.get_atdb_predecessor_task_id(self.work_spec))


class GetProcessingSite(unittest.TestCase):
    @mock.patch('lofardata.task.tasks.define_work_specification.delay', side_effect=mocked_delay)
    def test_get_processing_site_basic(self, mock_delay):
        self.work_spec = WorkSpecification.objects.create()
        self.site = ATDBProcessingSite()
        self.site.save()
        self.work_spec.processing_site = self.site
        self.work_spec.save()

        mock_delay.assert_called_with(self.work_spec.id)

        actual = helpers.get_processing_site(self.work_spec)
        expected = self.site
        self.assertEqual(actual, expected, "Retrieving basic atdb processing site did not succeed")

    @mock.patch('lofardata.task.tasks.define_work_specification.delay', side_effect=mocked_delay)
    def test_get_processing_site_missing_site(self, mock_delay):
        self.work_spec = WorkSpecification.objects.create()
        self.work_spec.save()
        mock_delay.assert_called_with(self.work_spec.id)

        self.assertRaises(WorkSpecificationNoSite, lambda: helpers.get_processing_site(self.work_spec))


class GetSurls(unittest.TestCase):
    @mock.patch('lofardata.task.tasks.define_work_specification.delay', side_effect=mocked_delay)
    def test_get_surls_single_entry(self, mock_delay):
        self.work_spec = WorkSpecification.objects.create()
        entry = [{"size": 3773,
                  "surl": 'srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/ops/projects/lc4_030/402998/L402998_summaryIS_f5baa34a.tar',
                  "type": "File", "location": "srm.grid.sara.nl"}]
        self.work_spec.inputs = {"surls": entry}
        self.work_spec.save()

        mock_delay.assert_called_with(self.work_spec.id)

        actual = helpers.get_surls(self.work_spec)
        expected = entry
        self.assertListEqual(actual, expected, "Retrieving single surl did not succeed")

    @mock.patch('lofardata.task.tasks.define_work_specification.delay', side_effect=mocked_delay)
    def test_get_surls_multiple_entries(self, mock_delay):
        self.work_spec = WorkSpecification.objects.create()
        entry_list = [{"size": 3773,
                       "surl": 'srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/ops/projects/lc4_030/402998/L402998_summaryIS_f5baa34a.tar',
                       "type": "File", "location": "srm.grid.sara.nl"},
                      {"size": 1234,
                       "surl": 'srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/ops/projects/lc4_030/402998/wolverine_is_cool.tar',
                       "type": "File", "location": "srm.grid.sara.nl"}]
        self.work_spec.inputs = {"surls": entry_list}
        self.work_spec.save()

        mock_delay.assert_called_with(self.work_spec.id)

        actual = helpers.get_surls(self.work_spec)
        expected = entry_list
        self.assertListEqual(actual, expected, "Retrieving multiple surls did not succeed")

    @mock.patch('lofardata.task.tasks.define_work_specification.delay', side_effect=mocked_delay)
    def test_get_surls_no_inputs(self, mock_delay):
        self.work_spec = WorkSpecification.objects.create()
        self.work_spec.save()
        mock_delay.assert_called_with(self.work_spec.id)

        self.assertRaises(RequestNotOk, lambda: helpers.get_surls(self.work_spec))

    @mock.patch('lofardata.task.tasks.define_work_specification.delay', side_effect=mocked_delay)
    def test_get_surls_no_surls(self, mock_delay):
        self.work_spec = WorkSpecification.objects.create()
        self.work_spec.inputs = {"batman": [{"size": 123, "type": "File", "location": "srm.grid.sara.nl"}]}
        self.work_spec.save()
        mock_delay.assert_called_with(self.work_spec.id)

        self.assertRaises(RequestNotOk, lambda: helpers.get_surls(self.work_spec))

    @mock.patch('lofardata.task.tasks.define_work_specification.delay', side_effect=mocked_delay)
    def test_get_surls_empty(self, mock_delay):
        self.work_spec = WorkSpecification.objects.create()
        entry = []
        self.work_spec.inputs = {"surls": entry}
        self.work_spec.save()

        mock_delay.assert_called_with(self.work_spec.id)
        self.assertRaises(RequestNotOk, lambda: helpers.get_surls(self.work_spec))

class SetErroredWorkSpecification(unittest.TestCase):

    @mock.patch('lofardata.task.tasks.define_work_specification.delay', side_effect=mocked_delay)
    def setUp(self, mock_delay):
        self.work_spec = WorkSpecification.objects.create()
        self.work_spec.save()
        mock_delay.assert_called_with(self.work_spec.id)

    def test_basic_error_case(self):
        message = "Foo is Bar"
        helpers.set_errored_work_specification(self.work_spec, RequestNotOk, IndexError(message))
        self.assertEqual(self.work_spec.submission_status, SUBMISSION_STATUS.ERROR)
        self.assertEqual(self.work_spec.error_message, RequestNotOk.default_detail + "\n" + message)

class FilterDataProducts(dtest.TestCase):

    def setUp(self) -> None:

        self.test_dataproduct1 = DataProduct.objects.create(obs_id='12345', oid_source='SAS', dataproduct_source='lofar',
                         dataproduct_type='observation',
                         project='LT10_10',
                         location='lta-head.lofar.psnc.pl',
                         activity='secret stuff',
                         surl='srm://lta-head.lofar.psnc.pl:4884/subfolder/a_some_nice.tar',
                         filesize=40,
                         antenna_set='HBA Dual Inner',
                         instrument_filter='110-190 MHz',
                         dysco_compression=False)

        self.test_dataproduct2 = DataProduct.objects.create(obs_id='23456', oid_source='SAS', dataproduct_source='lofar',
                         dataproduct_type='observation',
                         project='LT10_10',
                         location='lta-head.lofar.psnc.uk',
                         activity='even more secret stuff',
                         surl='srm://lta-head.lofar.psnc.pl:4884/subfolder/b_some_nice.tar',
                         filesize=40,
                         antenna_set='HBA Dual Inner',
                         instrument_filter='110-190 MHz',
                         dysco_compression=False)

        self.test_dataproduct3 = DataProduct.objects.create(obs_id='23456', oid_source='SAS', dataproduct_source='lofar',
                         dataproduct_type='observation',
                         project='LT10_11',
                         location='lta-head.lofar.psnc.de',
                         activity='even more secret stuff',
                         surl='srm://lta-head.lofar.psnc.pl:4884/subfolder/c_some_nice.tar',
                         filesize=40,
                         antenna_set='HBA Dual Inner',
                         instrument_filter='110-190 MHz',
                         dysco_compression=True)

        self.test_dataproduct4 = DataProduct.objects.create(obs_id='23456', oid_source='SAS', dataproduct_source='lofar',
                         dataproduct_type='observation',
                         project='LT10_11',
                         location='lta-head.lofar.psnc.pl',
                         activity='even more secret stuff',
                         surl='srm://lta-head.lofar.psnc.pl:4884/subfolder/d_some_nice.tar',
                         filesize=40,
                         antenna_set='HBA Dual Outer',
                         instrument_filter='110-190 MHz',
                         dysco_compression=True)

        self.single_field_single_result_filter_expected = [self.test_dataproduct1]
        self.single_field_multiple_results_filter_expected = [self.test_dataproduct1, self.test_dataproduct2]
        self.multiple_fields_single_result_filter_expected = [self.test_dataproduct2]
        self.multiple_fields_multiple_results_filter_expected = [self.test_dataproduct2, self.test_dataproduct3]
        self.mix_of_everything_expected = [self.test_dataproduct1]


    def test_get_dataproducts_by_filters_single_field_single_result(self):
        actual = list(helpers.get_dataproducts_by_filters({'obs_id': 12345}, []))
        self.assertCountEqual(actual, self.single_field_single_result_filter_expected)

    def test_get_dataproducts_by_filters_single_field_multiple_results(self):
        actual = list(helpers.get_dataproducts_by_filters({'project': 'LT10_10'}, []))
        self.assertCountEqual(actual, self.single_field_multiple_results_filter_expected)

    def test_get_dataproducts_by_filters_single_field_no_results(self):
        actual = list(helpers.get_dataproducts_by_filters({'project': 'xxx'}, []))
        self.assertFalse(actual)

    def test_get_dataproducts_by_filters_multiple_fields_single_result(self):
        actual = list(helpers.get_dataproducts_by_filters({'project': 'LT10_10', 'activity': 'even more secret stuff' }, []))
        self.assertCountEqual(actual, self.multiple_fields_single_result_filter_expected)

    def test_get_dataproducts_by_filters_multiple_fields_multiple_results(self):
        actual = list(helpers.get_dataproducts_by_filters({'antenna_set': 'HBA Dual Inner', 'activity': 'even more secret stuff'}, []))
        self.assertCountEqual(actual, self.multiple_fields_multiple_results_filter_expected)

    def test_get_dataproducts_by_filters_multiple_fields_no_results(self):
        actual = list(helpers.get_dataproducts_by_filters({'project': 'LT10_10', 'obs_id': -10}, []))
        self.assertFalse(actual)

    def test_get_dataproducts_by_filters_mix_of_everything(self):
        filter_definitions = [
            DataProductFilter(
                field='antenna_set',
                lookup_type='exact',
                filter_inclusion_type=DataFilterInclusionType.INCLUDE,
                filter_cardinality_type=DataFilterCardinalityType.SINGLE),
            DataProductFilter(
                field='location',
                lookup_type='endswith',
                filter_inclusion_type=DataFilterInclusionType.EXCLUDE,
                filter_cardinality_type=DataFilterCardinalityType.MULTIPLE),
        ]

        actual = list(helpers.get_dataproducts_by_filters({'antenna_set': 'HBA Dual Inner', 'location': '.uk\r\n.de'}, filter_definitions))
        self.assertCountEqual(actual, self.mix_of_everything_expected)

    def test_get_dataproducts_by_filters_no_filters_specified_error(self):
        with self.assertRaises(ValueError):
            helpers.get_dataproducts_by_filters(None, [])

