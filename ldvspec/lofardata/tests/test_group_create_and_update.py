import types
from unittest import mock

import django.test as dtest

from django.contrib.auth.models import User
from lofardata.models import WorkSpecification, Group, ATDBProcessingSite, DataProductFilter, SUBMISSION_STATUS
from lofardata.view_helpers.specification import create_work_specifications_for_group, \
    create_work_specification_for_group, update_work_specifications_in_group


class WorkSpecificationCreationDatabaseInteraction(dtest.TestCase):
    def setUp(self):
        self.user, _ = User.objects.get_or_create()
        self.site, _ = ATDBProcessingSite.objects.get_or_create(
            name="marvel", url="http://marvel.com", access_token="dummy"
        )
        self.group, _ = Group.objects.get_or_create(name="x-men", processing_site=self.site, selected_workflow="claw",
                                                    selected_workflow_tag="attack", task_filter="xavier")

        self.delay_return_value = types.SimpleNamespace(id='5')

    @mock.patch('lofardata.task.tasks.define_work_specification.delay')
    def test_create_group_with_one_obs_id(self, mock_delay):
        mock_delay.return_value = self.delay_return_value
        create_work_specification_for_group(self.group, self.user, "123", 4)

        work_specification = list(WorkSpecification.objects.filter(group=self.group.id))

        self.assertEqual(len(work_specification), 1)

        actual_inherited_group_values = [(x.processing_site, x.selected_workflow, x.selected_workflow_tag, x.task_filter) for x in
                                         work_specification if x.group.pk == self.group.pk]

        for actual_inherited_group_value in actual_inherited_group_values:
            self.assertTupleEqual(actual_inherited_group_value,
                                  (self.site, self.group.selected_workflow, self.group.selected_workflow_tag, self.group.task_filter))

    @mock.patch('lofardata.task.tasks.define_work_specification.delay')
    def test_create_group_with_multiple_obs_ids(self, mock_delay):
        mock_delay.return_value = self.delay_return_value
        create_work_specifications_for_group(self.group, self.user, ["123", "456", " 781 "], 3)

        work_specification = list(WorkSpecification.objects.filter(group=self.group.id))

        self.assertEqual(len(work_specification), 3)

        actual_inherited_group_values = [(x.processing_site, x.selected_workflow, x.selected_workflow_tag, x.task_filter) for x in
                                         work_specification if x.group.pk == self.group.pk]

        for actual_inherited_group_value in actual_inherited_group_values:
            self.assertTupleEqual(actual_inherited_group_value,
                                  (self.site, self.group.selected_workflow, self.group.selected_workflow_tag, self.group.task_filter))

    @mock.patch('lofardata.task.tasks.define_work_specification.delay')
    def test_create_group_with_filters(self, mock_delay):
        mock_delay.return_value = self.delay_return_value
        obs_ids = ['123', '456']
        create_work_specifications_for_group(self.group, self.user, obs_ids, 1)

        work_specification = list(WorkSpecification.objects.filter(group=self.group.id))

        actual_obs_ids = [x.filters['obs_id'] for x in work_specification if x.filters['obs_id'] in obs_ids]

        # assertCountEqual: Asserts that two iterables have the same elements, the same number of
        # times, without regard to order.
        self.assertCountEqual(actual_obs_ids, obs_ids)

    @mock.patch('lofardata.task.tasks.define_work_specification.delay')
    def test_create_group_batch_size(self, mock_delay):
        mock_delay.return_value = self.delay_return_value
        obs_ids = ['123', '456']
        create_work_specifications_for_group(self.group, self.user, obs_ids, 7)

        work_specifications = WorkSpecification.objects.filter(group=self.group.id)

        for work_specification in work_specifications:
            self.assertEqual(work_specification.batch_size, 7)


    @mock.patch("lofardata.view_helpers.specification.set_post_submit_values")
    @mock.patch('lofardata.task.tasks.define_work_specification.delay')
    def test_create_group_with_post_submit_values(self, mock_delay, post_submit_mock):
        mock_delay.return_value = self.delay_return_value
        create_work_specifications_for_group(self.group, self.user, ["123"], 1)
        self.assertTrue(post_submit_mock.called)

    @mock.patch("lofardata.view_helpers.specification.set_post_submit_values")
    @mock.patch('lofardata.task.tasks.define_work_specification.delay')
    def test_update_group_with_post_submit_values(self, mock_delay, post_submit_mock):
        mock_delay.return_value = self.delay_return_value
        work_specs = create_work_specifications_for_group(self.group, self.user, ["789"], 1)

        for work_spec in work_specs:
            work_spec.submission_status = SUBMISSION_STATUS.READY
            work_spec.save()

        update_work_specifications_in_group(self.group, self.user)
        self.assertEqual(post_submit_mock.call_count, 2)

    @mock.patch('lofardata.task.tasks.define_work_specification.delay')
    def test_update_group_should_not_update_not_editable_work_specs(self, mock_delay):
        mock_delay.return_value = self.delay_return_value

        not_editable_work_specs = create_work_specifications_for_group(self.group, self.user, ["11",  "13"], 1)

        editable_work_specs = create_work_specifications_for_group(self.group, self.user, ["12",  "14"], 1)

        for work_spec in editable_work_specs:
            work_spec.submission_status = SUBMISSION_STATUS.READY
            work_spec.save()

        updated_group = self.group
        updated_group.processing_site = None
        updated_group.selected_workflow = '456'
        updated_group.selected_workflow_tag = '789'
        updated_group.task_filter = 'magneto'

        update_work_specifications_in_group(self.group, self.user)

        for spec in not_editable_work_specs:
            spec.refresh_from_db()
            self.assertNotEqual(spec.processing_site, self.group.processing_site)
            self.assertNotEqual(spec.selected_workflow, self.group.selected_workflow)
            self.assertNotEqual(spec.selected_workflow_tag, self.group.selected_workflow_tag)
            self.assertNotEqual(spec.task_filter, self.group.task_filter)

        for spec in editable_work_specs:
            spec.refresh_from_db()
            self.assertEqual(spec.processing_site, self.group.processing_site)
            self.assertEqual(spec.selected_workflow, self.group.selected_workflow)
            self.assertEqual(spec.selected_workflow_tag, self.group.selected_workflow_tag)
            self.assertEqual(spec.task_filter, self.group.task_filter)

    @mock.patch('lofardata.task.tasks.define_work_specification.delay')
    def test_create_group_filter_propagation(self, mock_delay):
        mock_delay.return_value = self.delay_return_value

        # Create the standard set of filter fields (might not get propagated otherwise)
        for field in ["obs_id", "activity", "project", "dataproduct_type", "location"]:
            DataProductFilter.objects.create(
                field=field, name="Not Important", lookup_type="exact"
            )

        filters = {"activity": "Writing Awesome Tests"}
        self.group.filters = filters

        obs_ids = ["123", "456"]
        create_work_specifications_for_group(self.group, self.user, obs_ids, 0)

        work_specifications = WorkSpecification.objects.filter(group=self.group.id)

        for work_specification in work_specifications:
            self.assertEqual(work_specification.filters.get("activity"), filters.get("activity"))
