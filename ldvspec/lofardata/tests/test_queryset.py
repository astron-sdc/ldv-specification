import django.test as dtest

from lofardata.filters import get_submittable_workspecifications
from lofardata.models import WorkSpecification
from lofardata.models import SUBMISSION_STATUS


class TestQuerySet(dtest.TestCase):

    def setUp(self):
        input_with_surls = {
            "surls": [
                {"surl": "uno", "size": 0},
                {"surl": "dos", "size": 1},
                {"surl": "tres", "size": 1}
            ]
        }

        inputs_without_file_size = {
            "surls": [
                {"surl": "quatro", "size": 0},
            ]
        }

        WorkSpecification.objects.get_or_create(pk=1, submission_status=SUBMISSION_STATUS.READY,
                                                async_task_result=1)
        WorkSpecification.objects.get_or_create(pk=2, submission_status=SUBMISSION_STATUS.READY,
                                                inputs=input_with_surls, async_task_result=1)
        WorkSpecification.objects.get_or_create(pk=3, submission_status=SUBMISSION_STATUS.DEFINING, async_task_result=1)
        WorkSpecification.objects.get_or_create(pk=4, submission_status=SUBMISSION_STATUS.DEFINING,
                                                inputs=input_with_surls, async_task_result=1)
        WorkSpecification.objects.get_or_create(pk=5, submission_status=SUBMISSION_STATUS.DEFINING, async_task_result=1)
        WorkSpecification.objects.get_or_create(pk=6, submission_status=SUBMISSION_STATUS.ERROR, async_task_result=1)
        WorkSpecification.objects.get_or_create(pk=7, submission_status=SUBMISSION_STATUS.SUBMITTED,
                                                async_task_result=1)
        WorkSpecification.objects.get_or_create(pk=8, submission_status=SUBMISSION_STATUS.SUBMITTED,
                                                async_task_result=1)
        WorkSpecification.objects.get_or_create(pk=9, submission_status=SUBMISSION_STATUS.SUBMITTED,
                                                inputs=inputs_without_file_size, async_task_result=1)

    def test_get_submittable_workspecifications_status(self):
        actual = get_submittable_workspecifications(WorkSpecification.objects)
        for workspecification in actual:
            self.assertNotIn(workspecification.submission_status,
                             [SUBMISSION_STATUS.DEFINING, SUBMISSION_STATUS.SUBMITTED])

    def test_get_submittable_workspecifications_inputs(self):
        actual = get_submittable_workspecifications(WorkSpecification.objects)
        for workspecification in actual:
            self.assertNotEqual(len(workspecification.inputs['surls']), 0)

    def test_get_submittable_workspecifications_no_file_size(self):
        actual = get_submittable_workspecifications(WorkSpecification.objects)
        self.assertEqual(len(actual), 1)
