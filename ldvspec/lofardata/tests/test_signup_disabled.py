from rest_framework import test as rtest
from rest_framework import status

class TestSignupDisabled(rtest.APITestCase):

    def test_get404(self):
        response = self.client.get('/ldvspec/accounts/signup')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)