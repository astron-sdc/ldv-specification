import unittest

from django.contrib.auth.models import User

from lofardata.models import WorkSpecification
from lofardata.view_helpers.specification import set_post_submit_values


class WorkSpecificationCreation(unittest.TestCase):
    def test_set_created_by_when_already_set(self):
        existing_user = User(pk=1, username='existing')
        new_user = User(pk=2, username='new')

        specification = WorkSpecification(created_by=existing_user)
        set_post_submit_values(specification, new_user)

        self.assertEqual(specification.created_by, existing_user)

    def test_set_created_by_when_not_set(self):
        new_user = User(pk=2, username='new')

        specification = WorkSpecification()
        set_post_submit_values(specification, new_user)

        self.assertEqual(specification.created_by, new_user)