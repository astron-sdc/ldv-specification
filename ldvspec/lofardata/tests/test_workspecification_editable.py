import unittest

from lofardata.models import WorkSpecification, SUBMISSION_STATUS

class WorkSpecificationEditable(unittest.TestCase):
    def test_not_editable_submitted(self):
        specification = WorkSpecification(submission_status=SUBMISSION_STATUS.SUBMITTED)
        self.assertFalse(specification.is_editable())

    def test_not_editable_defining(self):
        specification = WorkSpecification(submission_status=SUBMISSION_STATUS.DEFINING)
        self.assertFalse(specification.is_editable())

    def test_editable_ready(self):
        specification = WorkSpecification(submission_status=SUBMISSION_STATUS.READY)
        self.assertTrue(specification.is_editable())

    def test_editable_error(self):
        specification = WorkSpecification(submission_status=SUBMISSION_STATUS.ERROR)
        self.assertTrue(specification.is_editable())

    def test_not_editable_loading(self):
        specification = WorkSpecification(submission_status=SUBMISSION_STATUS.LOADING)
        self.assertFalse(specification.is_editable())