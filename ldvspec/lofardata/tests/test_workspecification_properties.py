import unittest

from lofardata.models import WorkSpecification, SUBMISSION_STATUS


class WorkSpecificationShouldAutoSubmit(unittest.TestCase):
    @unittest.mock.patch('lofardata.models.WorkSpecification.get_file_info')
    def test_submission_status_ready_auto_submit_with_files(self, mock_file_info):
        mock_file_info.__get__ = unittest.mock.Mock(return_value=[10, 10, 10])
        work_spec = WorkSpecification(submission_status=SUBMISSION_STATUS.READY,
                                      is_auto_submit=True)
        self.assertTrue(work_spec.should_auto_submit)

    def test_submission_status_loading(self):
        work_spec = WorkSpecification(submission_status=SUBMISSION_STATUS.LOADING)
        self.assertFalse(work_spec.should_auto_submit)

    def test_submission_status_ready_no_auto_submit(self):
        work_spec = WorkSpecification(submission_status=SUBMISSION_STATUS.READY)
        self.assertFalse(work_spec.should_auto_submit)

    def test_submission_status_ready_auto_submit_no_files(self):
        work_spec = WorkSpecification(submission_status=SUBMISSION_STATUS.READY,
                                      is_auto_submit=True)
        self.assertFalse(work_spec.should_auto_submit)

    def test_submission_status_defining(self):
        work_spec = WorkSpecification(submission_status=SUBMISSION_STATUS.DEFINING)
        self.assertFalse(work_spec.should_auto_submit)

    def test_submission_status_submitted(self):
        work_spec = WorkSpecification(submission_status=SUBMISSION_STATUS.SUBMITTED)
        self.assertFalse(work_spec.should_auto_submit)

    def test_submission_status_error(self):
        work_spec = WorkSpecification(submission_status=SUBMISSION_STATUS.ERROR)
        self.assertFalse(work_spec.should_auto_submit)


class WorkSpecificationDidSubmissionStatusChange(unittest.TestCase):
    def test_change(self):
        work_spec = WorkSpecification(submission_status=SUBMISSION_STATUS.READY)
        work_spec.submission_status = SUBMISSION_STATUS.LOADING
        self.assertTrue(work_spec.did_submission_status_change)

    def test_no_change(self):
        work_spec = WorkSpecification()
        self.assertFalse(work_spec.did_submission_status_change)

    def test_no_change_with_setters(self):
        work_spec = WorkSpecification(submission_status=SUBMISSION_STATUS.READY)
        work_spec.submission_status = SUBMISSION_STATUS.READY
        self.assertFalse(work_spec.did_submission_status_change)
