from unittest import mock

import django.test as dtest
from lofardata.models import SUBMISSION_STATUS, ATDBProcessingSite, WorkSpecification, DataProduct
from lofardata.task.tasks import insert_task_into_atdb, define_work_specification
from lofardata.task.session_store import SessionStore

from lofardata.tests.util import mocked_delay, SessionMock

SessionStore.get_session()


@mock.patch("lofardata.task.tasks.define_work_specification.delay", side_effect=mocked_delay)
class DefineWorkSpecification(dtest.TestCase):
    def setUp(self):
        self.site: ATDBProcessingSite = ATDBProcessingSite.objects.create(
            name="x-men", url="https://marvel.com/x-men/", access_token="Wolverine"
        )

        self.work_spec: WorkSpecification = WorkSpecification(id=1, processing_site=self.site,
                                                              filters={"obs_id": "1337"})
        self.dataproduct = dict(obs_id='1337', oid_source='SAS', dataproduct_source='lofar',
                                dataproduct_type='observation',
                                project='LT10_10_Classified', location='lta-head.lofar.psnc.pl',
                                activity='secret stuff',
                                surl='srm://lta-head.lofar.psnc.pl:4884/x-men/wolverine.tar', filesize=40,
                                antenna_set='HBA Dual Inner', instrument_filter='110-190 MHz', dysco_compression=False)
        DataProduct.objects.create(**self.dataproduct)

    def test_statuses(self, mock_delay):
        self.assertEqual(self.work_spec.submission_status, SUBMISSION_STATUS.LOADING)
        self.work_spec.save()
        define_work_specification(1)
        self.work_spec.refresh_from_db()
        self.assertEqual(self.work_spec.submission_status, SUBMISSION_STATUS.READY)

    def test_update_inputs(self, mock_delay):
        self.work_spec.save()
        define_work_specification(1)
        self.work_spec.refresh_from_db()
        self.assertEqual(self.work_spec.inputs,
                         {'surls': [{'size': 40, 'surl': 'srm://lta-head.lofar.psnc.pl:4884/x-men/wolverine.tar'}]})

        # add another dataproduct for the same filter
        self.dataproduct['surl'] = 'srm://lta-head.lofar.psnc.pl:4884/x-men/hulk.tar'
        self.dataproduct['filesize'] = 120
        DataProduct.objects.create(**self.dataproduct)

        self.work_spec.save()
        define_work_specification(1)
        self.work_spec.refresh_from_db()
        self.assertEqual(self.work_spec.inputs,
                         {'surls': [{'size': 120, 'surl': 'srm://lta-head.lofar.psnc.pl:4884/x-men/hulk.tar'},
                                    {'size': 40, 'surl': 'srm://lta-head.lofar.psnc.pl:4884/x-men/wolverine.tar'}]})


@mock.patch.object(SessionStore, 'get_session', return_value=SessionMock())
@mock.patch("lofardata.task.tasks.define_work_specification.delay", side_effect=mocked_delay)
@mock.patch("lofardata.task.tasks.insert_task_into_atdb.delay")
class InsertTaskIntoATDB(dtest.TestCase):
    @mock.patch("lofardata.task.tasks.define_work_specification.delay", side_effect=mocked_delay)
    def setUp(self, mock_delay):
        self.site: ATDBProcessingSite = ATDBProcessingSite.objects.create(
            name="x-men", url="https://marvel.com/x-men/", access_token="Wolverine"
        )

        test_values = dict(obs_id='1337', oid_source='SAS', dataproduct_source='lofar',
                           dataproduct_type='observation',
                           project='lc0_012',
                           location='lta-head.lofar.psnc.pl',
                           activity='observation',
                           surl='srm://lta-head.lofar.psnc.pl:123/pnfs/grid.sara.nl/data/lofar/ops/projects/lc0_012/1337/very_nice.tar',
                           filesize=40,
                           antenna_set="HBA Dual Inner",
                           instrument_filter="110-190 MHz",
                           dysco_compression=True)

        dp = DataProduct(**test_values)
        dp.save()

        self.work_spec: WorkSpecification = WorkSpecification.objects.create(id=1, processing_site=self.site,
                                                                             filters={"obs_id": "1337"})
        define_work_specification(1)
        self.work_spec.refresh_from_db()

    def test_statuses(self, session_mock, mock_delay, mock_insert):
        self.assertEqual(self.work_spec.submission_status, SUBMISSION_STATUS.READY)
        insert_task_into_atdb(1)
        self.work_spec.refresh_from_db()
        self.assertEqual(self.work_spec.submission_status, SUBMISSION_STATUS.SUBMITTED)

    def test_with_batch_size(self, session_mock, mock_delay, mock_insert):
        self.work_spec.batch_size = 1
        insert_task_into_atdb(1)
        self.work_spec.refresh_from_db()
        self.assertEqual(self.work_spec.submission_status, SUBMISSION_STATUS.SUBMITTED)

    def test_with_predecessor(self, session_mock, mock_delay, mock_insert):
        predecessor: WorkSpecification = WorkSpecification.objects.create(id=2, processing_site=self.site,
                                                                          filters={"obs_id": "1337"})

        self.work_spec.predecessor_specification = predecessor
        insert_task_into_atdb(1)
        self.work_spec.refresh_from_db()
        self.assertEqual(self.work_spec.submission_status, SUBMISSION_STATUS.SUBMITTED)
