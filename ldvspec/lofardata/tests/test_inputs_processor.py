import unittest

from lofardata.view_helpers.inputs_processor import compute_inputs_histogram, compute_size_of_inputs


class ComputeInputSizes(unittest.TestCase):
    def test_input_sizes(self):
        test_data = {
            'only_a_file': {'class': 'File', 'size': 2},
            'a_list_of_files': [{'class': 'File', 'size': 2}, {'class': 'File', 'size': 5}],
            'nested_files': [{'item1': {'class': 'File', 'size': 1}}, {'class': 'File', 'size': 4}],
            'not_a_file': 'bla'
        }
        total_size, number_of_files, average_file_size = compute_size_of_inputs(test_data)

        self.assertEqual(14, total_size)
        self.assertEqual(5, number_of_files)
        self.assertEqual(2.8, average_file_size)


class ComputeInputsHistogram(unittest.TestCase):
    def test_basic(self):
        test_data = {"surls": [{"size": 33832140800, "surl": "test"},
                               {"size": 21832140800, "surl": "test"},
                               {"size": 33835408000, "surl": "test"},
                               {"size": 53832140800, "surl": "test"},
                               {"size": 29832140800, "surl": "test"},
                               {"size": 30832140801, "surl": "test"}]}

        min_size, max_size, n_bins, counts, biggest_bucket, bins = compute_inputs_histogram(test_data)

        self.assertEqual(6, n_bins)
        self.assertEqual(21832140800, min_size)
        self.assertEqual(53832140800, max_size)
        self.assertEqual(2, biggest_bucket)
        self.assertListEqual([1, 2, 2, 0, 0, 1], counts)
        self.assertListEqual(['20.333GB', '25.300GB', '30.267GB', '35.234GB', '40.201GB', '45.168GB', '50.135GB'], bins)


    def test_single_range(self):
        test_data = {"surls": [{"size": 1, "surl": "test"},
                               {"size": 1, "surl": "test"},
                               {"size": 1, "surl": "test"},
                               {"size": 1, "surl": "test"},
                               {"size": 1, "surl": "test"},
                               {"size": 1, "surl": "test"}]}

        min_size, max_size, n_bins, counts, biggest_bucket, bins = compute_inputs_histogram(test_data)

        self.assertEqual(1, n_bins)
        self.assertEqual(1, min_size)
        self.assertEqual(1, max_size)
        self.assertEqual(6, biggest_bucket)
        self.assertListEqual([6], counts)
        self.assertListEqual(['0.500B', '1.500B'], bins)


    def test_extreme_wide_range(self):
        test_data = {"surls": [{"size": 1, "surl": "test"},
                               {"size": 100000, "surl": "test"},
                               {"size": 100000000, "surl": "test"},
                               {"size": 100000000000, "surl": "test"},
                               {"size": 1000000000000000, "surl": "test"},
                               {"size": 1000000000000000000, "surl": "test"}]}

        min_size, max_size, n_bins, counts, biggest_bucket, bins = compute_inputs_histogram(test_data)

        self.assertEqual(6, n_bins)
        self.assertEqual(1, min_size)
        self.assertEqual(1000000000000000000, max_size)
        self.assertEqual(5, biggest_bucket)
        self.assertListEqual([5, 0, 0, 0, 0, 1], counts)  # TODO: if this is the case, adapt it to logarithmic scale
        self.assertListEqual(['1.000B', '148.030PB', '296.059PB', '444.089PB', '592.119PB', '740.149PB', '888.178PB'],
                             bins)


    def test_two_ranges(self):
        test_data = {"surls": [{"size": 97873920, "surl": "test"},
                               {"size": 97873920, "surl": "test"},
                               {"size": 97873920, "surl": "test"},
                               {"size": 97873920, "surl": "test"},
                               {"size": 97873920, "surl": "test"},
                               {"size": 97873920, "surl": "test"},
                               {"size": 97873920, "surl": "test"},
                               {"size": 97873920, "surl": "test"},
                               {"size": 97884160, "surl": "test"},
                               {"size": 97884160, "surl": "test"},
                               {"size": 97884160, "surl": "test"},
                               {"size": 97884160, "surl": "test"},
                               {"size": 97884160, "surl": "test"},
                               {"size": 97884160, "surl": "test"},
                               {"size": 97884160, "surl": "test"}]}

        min_size, max_size, n_bins, counts, biggest_bucket, bins = compute_inputs_histogram(test_data)

        self.assertEqual(2, n_bins)
        self.assertEqual(97873920, min_size)
        self.assertEqual(97884160, max_size)
        self.assertEqual(8, biggest_bucket)
        self.assertListEqual([8, 7], counts)  # TODO: if this is the case, adapt it to logarithmic scale
        self.assertListEqual(['93.340MB', '93.345MB', '93.350MB'], bins)
