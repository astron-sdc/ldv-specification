from django import template
from django.contrib.auth.models import User

from lofardata.models import WorkSpecification

register = template.Library()

@register.simple_tag
def can_be_viewed_by(workspecification: WorkSpecification, user: User) -> bool:
    return workspecification.can_be_viewed_by(user)

@register.simple_tag
def can_be_edited_by(workspecification: WorkSpecification, user: User) -> bool:
    return workspecification.can_be_edited_by(user)

@register.simple_tag
def can_be_submitted_by(workspecification: WorkSpecification, user: User) -> bool:
    return workspecification.can_be_submitted_by(user)