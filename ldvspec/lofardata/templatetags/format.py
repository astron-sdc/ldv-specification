from django import template

register = template.Library()


@register.simple_tag
def format_to_percentage(val):
    return '{:.0%}'.format(val)


@register.simple_tag
def format_size(num, suffix="B"):
    if num == 0:
        return "-"
    for unit in ["", "K", "M", "G", "T", "P", "E", "Z"]:
        if abs(num) < 1024.0:
            return f"{num:2.1f}{unit}{suffix}"
        num /= 1024.0
    return f"{num:.1f}Yi{suffix}"
