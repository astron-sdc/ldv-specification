class AutoRefresher {
    getRowEditButtonEnabledHtml(id) {
        const idParam = `/${id}/`;
        return `<a class="button--icon-button"
                   href="${this.specification_update_uri.replace(/\/0\/$/, idParam)}"
                   title="Edit this work specification">
                   <span class="icon icon--pen"></span>
                </a>`;
    }

    #rowEditButtonLoadingHtml = ` 
                <a class="link--disabled button--icon-button"
                   title="Still retrieving the necessary information">
                   <span class="icon icon--color-inherit icon--pen">
                        <span class="icon icon--color-inherit icon--stacked icon--secondary icon--hourglass-full"></span>
                   </span></a>`;

    #rowEditButtonSubmittedHtml = `
                <a class="link--disabled button--icon-button"
                   title="Tasks already present in ATDB">
                   <span class="icon icon--color-inherit icon--pen">
                       <span class="icon icon--color-inherit icon--stacked icon--success icon--check"></span>
                   </span></a>`;

    #submitButtonNoFilesHtml = `
                <a class="link--disabled button--icon-button"
                   title="Cannot submit this task to ATDB since it does not contain any files">
                   <span class="icon icon--color-inherit icon--play">
                        <span class="icon icon--color-inherit icon--stacked icon--primary icon--file"></span>
                   </span>
                </a>`;

    #getSubmitButtonEnabledHtml(id) {
        return `
                <button type="button"
                         id="submit-button-icon-${id}"
                        class="custom--action-button button button--icon-button"
                        title="Submit to ATDB">
                        <span class="icon icon--play"></span>
                </button>`;
    }

    #submitButtonLoadingHtml = `
                <a class="link--disabled button--icon-button"
                   title="Still retrieving the necessary information">
                   <span class="icon icon--color-inherit icon--play">
                        <span class="icon icon--color-inherit icon--stacked icon--secondary icon--hourglass-full"></span>
                   </span>
                </a>`;

    #submitButtonSubmittedHtml = `
                <a class="link--disabled button--icon-button"
                   title="Tasks already present in ATDB">
                   <span class="icon icon--color-inherit icon--play">
                        <span class="icon icon--color-inherit icon--stacked icon--success icon--check"></span>
                   </span>
                </a>`;

    constructor(WorkSpecification, document, apiUri, atdb_task_uri, specification_update_uri) {
        this.WorkSpecification = WorkSpecification;
        this.document = document;
        this.apiUri = apiUri;
        this.atdb_task_uri = atdb_task_uri;
        this.specification_update_uri = specification_update_uri;
    }

    startAutoRefresh({interval = 5000}) {
        setInterval(async () => {
            await this.refreshWorkSpecifications();
        }, interval);
    }

    async refreshWorkSpecifications() {
        const workSpecifications = await this.fetchWorkSpecifications();
        for (const workSpecification of workSpecifications) {
            this.updateWorkSpecificationElements(workSpecification);
        }
    }

    async fetchWorkSpecifications() {
        const uri = `${this.apiUri}/workspecification/enriched/`;
        const response = await fetch(uri);
        const results = await response.json();
        return results.map(spec => Object.assign(new this.WorkSpecification(), spec));
    }

    getRowElementForId(id) {
        return this.document.getElementById(`specification-${id}`);
    }

    updateWorkSpecificationElements(workSpecification) {
        const rowElement = this.getRowElementForId(workSpecification.id);
        if (rowElement) {
            this.updateSubmissionStatusElement(workSpecification, rowElement)
            this.updateAtdbTasksElement(workSpecification, rowElement);
            this.updateFileInfoElements(workSpecification, rowElement);
            this.updateSplitTaskElement(workSpecification, rowElement);
            this.updateRowEditButtonElement(workSpecification, rowElement);
            this.updateRowSubmitButtonElement(workSpecification, rowElement);
        }
    }

    updateSubmissionStatusElement(workSpecification, rowElement) {
        const statusBadgeElement = rowElement.querySelector('div.badge');
        const { statusText, badgeColor, title } = workSpecification.getStatusValues();

        statusBadgeElement.innerText = statusText;
        statusBadgeElement.className = statusBadgeElement.className.replace(/(^|\s)badge-\S+/g, `$1badge--${badgeColor}`);
        statusBadgeElement.title = title;
    }

    updateAtdbTasksElement(workSpecification, rowElement) {
        const atdbTasksElement = rowElement.querySelector('div.atdb-tasks');
        if (workSpecification.atdb_task_ids && workSpecification.atdb_task_ids.length) {
            const idParam = `/${workSpecification.id}/`;
            atdbTasksElement.innerHTML = `<a class="button button--icon-button custom--button-no-padding" href="${this.atdb_task_uri.replace(/\/0\/$/, idParam)}">
                <span class="icon icon--list-ul"></span>
            </a>`;
        } else {
             atdbTasksElement.innerHTML = '<div class="text">-</div>';
        }
    }

    updateFileInfoElements(workSpecification, rowElement) {
          const totalSizeElement = rowElement.querySelector('div.total-size');
          totalSizeElement.innerText = workSpecification.file_info.formatted_total_size;

          const sizePerTaskElement = rowElement.querySelector('div.size-per-task');
          sizePerTaskElement.innerText = workSpecification.batch_size ?
              workSpecification.file_info.formatted_average_file_size : workSpecification.file_info.formatted_total_size;

          const totalFilesElement = rowElement.querySelector('div.total-files');
          totalFilesElement.innerText = workSpecification.file_info.number_of_files;

          const filesPerTaskElement = rowElement.querySelector('div.files-per-task');
          filesPerTaskElement.innerText = workSpecification.batch_size || workSpecification.file_info.number_of_files;
    }

    updateSplitTaskElement(workSpecification, rowElement) {
          const splitTaskElement = rowElement.querySelector('div.split');
          if (workSpecification.batch_size) {
              splitTaskElement.innerHTML = `<span class="icon icon--check"
                  title="The work specification has been split in multiple tasks for ATDB"></span>`;
          } else if (workSpecification.file_info.average_file_size > 8000000000 && workSpecification.file_info.number_of_files > 1) {
              splitTaskElement.innerHTML = `<span class="icon icon--error icon--times"
                  title="The inputs size exceeds the threshold of 8GB and it contains multiple files, so splitting is recommended"></span>`;
          } else {
              splitTaskElement.innerHTML = `<span class="icon icon--times"
                  title="The work specification is not split, but this is also not recommended since it does not exceed the threshold of 8GB"></span>`;
          }
    }

    updateRowEditButtonElement(workSpecification, rowElement) {
         const rowEditButtonElement = rowElement.querySelector('div.row-edit-button');

         if (rowEditButtonElement) {
             if (workSpecification.is_editable) {
                 rowEditButtonElement.innerHTML = this.getRowEditButtonEnabledHtml(workSpecification.id);
             } else {
                 if (workSpecification.submission_status === "L") {
                     rowEditButtonElement.innerHTML = this.#rowEditButtonLoadingHtml;
                 } else {
                     rowEditButtonElement.innerHTML = this.#rowEditButtonSubmittedHtml;
                 }
             }
         }
    }

    updateRowSubmitButtonElement(workSpecification, rowElement) {
         const rowSubmitButtonElement = rowElement.querySelector('div.row-submit-button');

         if (workSpecification.is_editable) {
            const numberOfFiles = workSpecification.file_info?.number_of_files ?? 0;

            if (numberOfFiles) {
                rowSubmitButtonElement.innerHTML = this.#getSubmitButtonEnabledHtml(workSpecification.id);
            } else {
                rowSubmitButtonElement.innerHTML = this.#submitButtonNoFilesHtml;
            }
         } else {
               if (workSpecification.submission_status === "L") {
                    rowSubmitButtonElement.innerHTML = this.#submitButtonLoadingHtml;
               } else {
                    rowSubmitButtonElement.innerHTML = this.#submitButtonSubmittedHtml;
               }
         }
    }

    setSubmitButtonToLoading(id) {
        const rowElement = this.getRowElementForId(id);
        if (rowElement) {
            const rowSubmitButtonElement = rowElement.querySelector('div.row-submit-button');
            rowSubmitButtonElement.innerHTML = this.#submitButtonLoadingHtml
        }
    }
    
    onSpecificationsDeleted(groupId, deletedIds) {
        if (groupId) {
            this.uncheckGroup(groupId);
        }

        for (const id of deletedIds) {
            this.removeRowAndGroupElements(id);
        }
    }

    onSpecificationsSubmitted(specificationIds, groupId) {
        if (groupId) {
            this.uncheckGroup(groupId);
        }

        for (const id of specificationIds) {
            this.setSubmitButtonToLoading(id);
        }
    }

    uncheckGroup(groupId) {
        const groupCheckbox = this.document.getElementById(`group-${groupId}`);
        groupCheckbox.checked = false;
        groupCheckbox.dispatchEvent(new Event('click'));
    }

    removeRowAndGroupElements(id) {
        const row = document.getElementById(`specification-${id}`);
        const groupElement = row.closest(".group-wrapper");

        if (row) {
            row.remove();
        }

        const remainingRows = groupElement.querySelectorAll('div.table__row[id^="specification-"]');
        if (remainingRows && !remainingRows.length) {
            groupElement.remove();
        }
    }
}


export default AutoRefresher;

