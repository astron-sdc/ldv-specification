class WorkSpecification {
    getStatusValues() {
        const statusObject = (statusText, badgeColor, title) => ({
          statusText: statusText,
          badgeColor: badgeColor,
          title: title,
        })

        switch (this.submission_status) {
            case "L":
                return statusObject(
                    "Loading",
                    "secondary",
                    "The work specification is now retrieving the necessary information."
                )
            case "R":
                const numberOfFiles = this.file_info?.number_of_files ?? 0;

                if (numberOfFiles === 0) {
                    return statusObject(
                        "Ready*",
                        "orange",
                        "The work specification has been created, but does not contain any files. Please inspect the specified filters."
                    )
                } else if (numberOfFiles > 0) {
                    return statusObject(
                        "Ready",
                        "primary",
                        "The work specification has been created, but has not yet been sent to ATDB for processing."
                    )
                }
                break;
            case "D":
                return statusObject(
                    "Defining",
                    "secondary",
                    "The work specification is now being sent to ATDB."
                )
            case "S":
                return statusObject(
                    "Submitted",
                    "green",
                    "The work specification has been succcesfully processed in ATDB."
                )
            case "E":
                return statusObject(
                    "Error",
                    "red",
                    "Retrieving the files to process or submitting the task to ATDB resulted in an error. Please reports this to the support helpdesk or try again."
                )
            default:
                throw Error(`Unknown value for ${this.submission_status}`);
        }
    }
}

export default WorkSpecification;