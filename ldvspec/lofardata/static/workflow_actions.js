function WorkflowActionsViewModel(csrf_token, delete_multiple_uri, submit_multiple_uri, onSpecificationsDeleted, onSpecificationsSubmitted) {
        const self = this;

        self.csrf_token = csrf_token;
        self.delete_multiple_uri = delete_multiple_uri;
        self.submit_multiple_uri = submit_multiple_uri;

        self.onSpecificationsDeleted = onSpecificationsDeleted;
        self.onSpecificationsSubmitted = onSpecificationsSubmitted;
        self.confirmDeleteMessage = "Are you sure you want to delete the selected work specifications? This cannot be undone.";

        self.selectedUngroupedSpecifications = ko.observableArray([]);

        self.selectedGroupedSpecifications = ko.observableArray([]);

        self.deleteSelectedUngroupedSpecifications = function() {
            const selectedIds = self.selectedUngroupedSpecifications();
            if (selectedIds.length && confirm(self.confirmDeleteMessage)) {
                self.callApi(self.delete_multiple_uri, { "ids": selectedIds }, (deletedIds) => {
                    self.onSpecificationsDeleted('ungrouped', deletedIds);
                });
            }
        }

        self.submitSelectedUngroupedSpecifications = function() {
            const selectedIds = self.selectedUngroupedSpecifications();
            if (selectedIds.length) {
                self.callApi(self.submit_multiple_uri, { "ids": selectedIds }, (submittedIds) => {
                    self.onSpecificationsSubmitted(submittedIds, 'ungrouped');
                });
            }
        }

        self.getSelectedGroupedIds = function(groupId) {
            const selectedIds =
                self.selectedGroupedSpecifications()
                    .map(value => value.split('-'))
                    .map(value => ({
                            groupId: value[0],
                            specificationId: value[1]
                        })
                    )
                    .filter(value => value.groupId == groupId)
                    .map(value => value.specificationId);

            return selectedIds;
        }

        self.deleteSelectedGroupSpecifications = function(groupId) {
            const selectedIds = self.getSelectedGroupedIds(groupId);

            if (selectedIds.length && confirm(self.confirmDeleteMessage)) {
                self.callApi(self.delete_multiple_uri, { "ids": selectedIds }, (deletedIds) => {
                    self.onSpecificationsDeleted(groupId, deletedIds);
                });
            }
        }

        self.submitSelectedGroupSpecifications = function(groupId) {
            const selectedIds = self.getSelectedGroupedIds(groupId);
            if (selectedIds.length) {
                self.callApi(self.submit_multiple_uri, { "ids": selectedIds }, (submittedIds) => {
                    self.onSpecificationsSubmitted(submittedIds, groupId);
                });
            }
        }

        self.deleteSingleSpecification = function(groupId, specificationId) {
            if (confirm(self.confirmDeleteMessage)) {
               self.callApi(self.delete_multiple_uri, { "ids": [specificationId] }, (deletedIds) => {
                    self.onSpecificationsDeleted(groupId, deletedIds);
                });
            }
        }

        self.submitSingleSpecification = function(specificationId) {
           self.callApi(self.submit_multiple_uri, { "ids": [specificationId] }, (submittedIds) => {
                self.onSpecificationsSubmitted(submittedIds);
            });
        }

        self.callApi = function(uri, data, success) {
            self.postData(uri, data)
            .then((response) => response.json())
            .then((data) => {
                if (success) {
                    success(data);
                }
            })
            .catch((error) => {
                console.log(error);
            });
        }

        self.postData = async function(uri = '', data = {}) {
            const response = await fetch(uri, {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json',
                  'x-csrftoken': self.csrf_token
                },
                body: JSON.stringify(data)
            });
            return response;
        }
    }

export default WorkflowActionsViewModel;