(function () {
    document.addEventListener("DOMContentLoaded", function (event) {
        ko.applyBindings(new SpecificationViewModel());
    });

    function SpecificationViewModel() {
        var self = this;

        self.pk = pk;
        self.existing_workflow = existingWorkflow;
        self.existing_workflow_tag = existingWorkflowTag;

        self.processingSites = ko.observableArray(
            processingSites
        );

        self.selectedProcessingSite = ko.observable(initialProcessingSite);

        self.selectedProcessingSite.subscribe((context) => {
            self.loadWorkflows();
        });

        self.groups = ko.observableArray(
            groups
        );

        self.selectedGroup = ko.observable(existingGroup);

        self.groupHasBeenSelectedAtLeastOnce = false;

        self.selectedGroup.subscribe((context) => {
            const group = self.groups() && self.groups().filter(group => group.id === self.selectedGroup())[0];
            if (group) {
                self.groupHasBeenSelectedAtLeastOnce = true;
                self.selectedProcessingSite(group.processing_site);
                self.selectedTag(group.selected_workflow_tag);
                self.selectedWorkflow(group.selected_workflow);
                if (!self.pk) {
                    self.taskFilter(group.task_filter);
                }
            } else {
                // setting the processing site to null will cause the loadWorkflows
                // function to trigger, which in turn will clear the previously selected
                // filled in workflow and tag
                if (!self.pk && self.groupHasBeenSelectedAtLeastOnce) {
                    // only do this for new work specifications (not existing ones)
                    // additionally, only do this when a group has already been selected once
                    // this is to avoid clearing the auto selected first processing site option
                    self.selectedProcessingSite(null);
                    self.taskFilter(null);
                }
            }
        });

        self.loadWorkflows = function () {

            const selectedProcessingSite = self.selectedProcessingSite();
            if (selectedProcessingSite) {
                const processingSiteUrl = processingSite.replace('replace', selectedProcessingSite);
                self.isLoading(true);

                fetch(processingSiteUrl).then((response) => response.json()).then((data) => {
                    const workflowUrl = data.url + 'workflows';

                    fetch(workflowUrl).then((response) => response.json()).then((data) => {

                        const workflows = data.results;
                        self.workflows(workflows);
                        const group = self.groups() && self.groups().filter(group => group.id === self.selectedGroup())[0];

                        if (group) {
                            self.selectedTag(group.selected_workflow_tag);
                            self.selectedWorkflow(group.selected_workflow);
                        } else {
                            // auto select first tag if there is only 1, and there is no tag saved on the work specification yet
                            const autoSelectedSingleTag = self.tags().length === 1 ? self.tags()[0] : null;
                            self.selectedTag(self.existing_workflow_tag || autoSelectedSingleTag);

                            // auto select first workflow if there is only 1, and there is no workflow saved on the work specification yet
                            const autoSelectedWorkflow = self.workflowsByTag().length === 1 ? self.workflowsByTag()[0].workflow_uri : null;
                            self.selectedWorkflow(self.existing_workflow || autoSelectedWorkflow);
                        }
                    }).finally(() => {
                        self.isLoading(false);
                    });
                }).catch(() => {
                    self.isLoading(false);
                });
            } else {
                self.workflows([]);
                self.selectedWorkflow(null);
                self.selectedTag(null);
            }
        }

        self.isLoading = ko.observable(false);

        self.caption = ko.computed(() => {
            return self.isLoading() ? 'Loading...' : '---------'
        });

        self.workflows = ko.observableArray();

        self.workflowsByTag = ko.computed(() => {
            const byTag = self.workflows().filter(workflow => workflow.tag === self.selectedTag());
            return byTag;
        });

        self.taskFilter = ko.observable(existingTaskFilter);

        self.tags = ko.computed(() => {
            const tags = self.workflows().map(workflow => workflow.tag || 'Untagged');
            return [...new Set(tags)];
        });

        self.selectedTag = ko.observable();
        self.selectedWorkflow = ko.observable();

        self.loadWorkflows();
    }
})();