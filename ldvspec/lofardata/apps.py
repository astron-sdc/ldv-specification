import sys

from django.apps import AppConfig

class LofardataConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'lofardata'