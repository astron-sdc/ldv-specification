from typing import List

from django.contrib.auth.models import User

from lofardata.models import Group, WorkSpecification


def set_post_submit_values(specification, user):
    specification.async_task_result = None
    if specification.created_by is None:
        specification.created_by = user


def split_obs_ids_string(obs_ids_string: str) -> List[str]:
    return [obs_id.strip() for obs_id in obs_ids_string.split(",")]


def create_work_specifications_for_group(group: Group, created_by: User, obs_ids: List[str], batch_size: int) -> List[
    WorkSpecification]:
    work_specs: List[WorkSpecification] = []

    for obs_id in obs_ids:
        work_specs.append(
            create_work_specification_for_group(group, created_by, obs_id, batch_size)
        )

    return work_specs


def propagate_group_properties_to_workspecification(group: Group, workspecification: WorkSpecification):
    workspecification.processing_site = group.processing_site
    workspecification.selected_workflow = group.selected_workflow
    workspecification.selected_workflow_tag = group.selected_workflow_tag
    workspecification.task_filter = group.task_filter

    # A WorkSpecification can not be its own predecessor
    if workspecification.pk is not group.predecessor_specification_id:
        workspecification.predecessor_specification = group.predecessor_specification

    # Reset group filters (excluding 'obs_id'), effectively allowing
    # to clear them after they have been set once
    for filter_field, _ in list(workspecification.filters.items()):
        if filter_field != "obs_id":
            workspecification.filters.pop(filter_field)

    # Groups will not contain `obs_id` filters (specifically) excluded
    if group.filters is not None:
        for filter_field in group.filters:
            workspecification.filters[filter_field] = group.filters[filter_field]


def create_work_specification_for_group(group: Group, created_by: User, obs_id: str,
                                        batch_size: int) -> WorkSpecification:
    workspecification = WorkSpecification(filters={"obs_id": obs_id}, group=group, batch_size=batch_size)

    propagate_group_properties_to_workspecification(group, workspecification)
    set_post_submit_values(workspecification, created_by)

    workspecification.save()
    return workspecification

def update_work_specifications_in_group(group, user):
    workspecifications_in_group = WorkSpecification.objects.filter(group=group)
    editable_specs = [spec for spec in workspecifications_in_group if spec.is_editable()]
    for workspecification in editable_specs:
        propagate_group_properties_to_workspecification(group, workspecification)
        set_post_submit_values(workspecification, user)
        workspecification.save()
