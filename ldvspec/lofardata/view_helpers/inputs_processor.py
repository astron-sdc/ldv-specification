from typing import Tuple

import numpy


def compute_size_of_inputs(inputs: dict) -> Tuple[int, int, int]:
    total_size = 0
    number_of_files = 0

    if isinstance(inputs, dict) and "size" in inputs:
        number_of_files = 1
        total_size = inputs["size"]
    elif (
            isinstance(inputs, dict)
            or isinstance(inputs, list)
            or isinstance(inputs, tuple)
    ):
        values = inputs
        if isinstance(inputs, dict):
            values = inputs.values()
        for value in values:
            item_total, item_count, _ = compute_size_of_inputs(value)
            total_size += item_total
            number_of_files += item_count

    average_file_size = total_size / number_of_files if number_of_files else 0

    return total_size, number_of_files, average_file_size


def compute_inputs_histogram(inputs):
    # create sizes array
    if isinstance(inputs, dict):
        inputs = inputs.values()
    inputs_sizes = []
    for entry in inputs:
        for item in entry:
            inputs_sizes.append(item['size'])
    inputs_sizes = numpy.array(inputs_sizes)

    # define histogram values
    min_size = inputs_sizes.min()
    max_size = inputs_sizes.max()

    n_distinct_sizes = numpy.unique(inputs_sizes).__len__()
    n_bins = min(n_distinct_sizes, 100)
    counts, buckets = numpy.histogram(inputs_sizes, bins=n_bins, range=(min_size, max_size))
    formatted_bins = [format_size(bucket) % bucket for bucket in buckets]

    return min_size, max_size, n_bins, counts.tolist(), counts.max(), formatted_bins


def format_size(num, suffix="B"):
    if num == 0:
        return "-"
    for unit in ["", "K", "M", "G", "T", "P", "E", "Z"]:
        if abs(num) < 1024.0:
            return f"{num:3.3f}{unit}{suffix}"
        num /= 1024.0
    return f"{num:.1f}Yi{suffix}"
