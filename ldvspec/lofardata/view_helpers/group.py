from django.db.models import Count
from lofardata.models import Group

def delete_empty_groups():
    Group.objects.all().annotate(count=Count('workspecification')).filter(count=0).delete()