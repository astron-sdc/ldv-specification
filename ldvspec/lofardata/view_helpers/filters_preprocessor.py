from typing import List, Optional, Union

from lofardata.models import DataFilterType, DataProductFilter, Group, WorkSpecification
from lofardata.view_helpers.dataproductinfo import get_distinct_dataproduct_field


def preprocess(filterable: Union[WorkSpecification, Group], exclude: Optional[List[str]] = None):
    """Preprocess available dataproduct filters and pre-fill them if the filterable
    objects already contains a value for the filter field.

        Parameters:
            filterable (Union[Workspecification, Group]: A WorkSpecification or Group to pre-fill the filter fields
           exclude (List[str]): which fields to exclude; if None, then all fields will be shown

        Examples:
            A new WorkSpecification without any fields would yield:
            preprocess(newSpec) -> DataProductFilter.objects.all()

            An existing WorkSpecification with some fields would yield:
            preprocess(newSpec) -> DataProductFilter.objects.all()
            where (if the field is set): field.default = WorkSpec.filters["field"]
    """

    if exclude is None:
        dataproduct_filters = DataProductFilter.objects.all()
    else:
        dataproduct_filters = DataProductFilter.objects.exclude(field__in=exclude)

    dataproduct_filters = dataproduct_filters.order_by('id')

    for dataproduct_filter in dataproduct_filters:
        if (
                filterable is not None
                and filterable.filters
                and dataproduct_filter.field in filterable.filters
        ):
            dataproduct_filter.default = filterable.filters[dataproduct_filter.field]
        else:
            dataproduct_filter.default = ""

        if dataproduct_filter.filter_type == DataFilterType.DROPDOWN:
            distinct_filter_fields = get_distinct_dataproduct_field(
                dataproduct_filter.field
            )
            dataproduct_filter.choices = [(field,) for field in distinct_filter_fields]

    return dataproduct_filters
