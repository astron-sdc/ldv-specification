from typing import List
from lofardata.task.tasks import insert_task_into_atdb


def submit_single_to_atdb(workspecification_id: int):
    submit_list_to_atdb([workspecification_id])


def submit_list_to_atdb(workspecification_ids: List[int]):
    for workspecification_id in workspecification_ids:
        insert_task_into_atdb.delay(workspecification_id)
