import logging

from celery.result import AsyncResult
from django.contrib.auth.models import User
from django.contrib.postgres.fields import ArrayField
from django.db import IntegrityError, models
from django.utils.translation import gettext_lazy as _
from django_prometheus.models import ExportModelOperationsMixin

from lofardata.templatetags.format import format_size
from lofardata.view_helpers import inputs_processor

logger = logging.getLogger(__name__)


class DataProduct(models.Model):
    obs_id = models.CharField(verbose_name="OBS_ID", max_length=15)
    oid_source = models.CharField(verbose_name="OBS_ID_SOURCE", max_length=15)
    dataproduct_source = models.CharField(max_length=20)
    dataproduct_type = models.CharField(max_length=50)
    project = models.CharField(max_length=50)
    location = models.CharField(max_length=50)
    activity = models.CharField(max_length=50)
    surl = models.CharField(max_length=200, unique=True)
    filesize = models.PositiveBigIntegerField()
    antenna_set = models.CharField(max_length=50, blank=True, null=True, default="")
    instrument_filter = models.CharField(max_length=50, blank=True, null=True, default="")
    dysco_compression = models.BooleanField(blank=True, null=True, default=False)

    @staticmethod
    def insert_dataproduct(
            obs_id,
            oid_source,
            dataproduct_source,
            dataproduct_type,
            project,
            location,
            activity,
            surl,
            filesize,
            antenna_set,
            instrument_filter,
            dysco_compression
    ):

        dp = DataProduct(
            obs_id=obs_id,
            oid_source=oid_source,
            dataproduct_source=dataproduct_source,
            dataproduct_type=dataproduct_type,
            project=project,
            location=location,
            activity=activity,
            filesize=filesize,
            antenna_set=antenna_set,
            instrument_filter=instrument_filter,
            dysco_compression=dysco_compression,
            surl=surl,
        )
        dp.save()
        return dp

    def save(self, *args, **kwargs):
        try:
            super(DataProduct, self).save(*args, **kwargs)
        except IntegrityError:
            logger.warning("Surl '{}' is already present".format(self.surl))
            pass


class DataFilterType(models.TextChoices):
    DROPDOWN = "Dropdown", _("Dropdown")
    FREEFORM = "Free", _("Freeform")

class DataFilterInclusionType(models.TextChoices):
    INCLUDE = "Include", _("Include")
    EXCLUDE = "Exclude", _("Exclude")

class DataFilterCardinalityType(models.TextChoices):
    SINGLE = "Single", _("Single")
    MULTIPLE = "Multiple", _("Multiple")

class DataProductFilter(models.Model):
    field = models.CharField(max_length=100)
    name = models.CharField(max_length=20)
    help_text = models.CharField(max_length=150, blank=True, default="")
    lookup_type = models.CharField(max_length=100)
    filter_type = models.CharField(max_length=20, choices=DataFilterType.choices, default=DataFilterType.FREEFORM)
    filter_inclusion_type = models.CharField(max_length=20, choices=DataFilterInclusionType.choices, default=DataFilterInclusionType.INCLUDE)
    filter_cardinality_type = models.CharField(max_length=20, choices=DataFilterCardinalityType.choices, default=DataFilterCardinalityType.SINGLE)

    def __str__(self):
        return f'({self.pk}) "{self.name}" [{self.field} ->{self.lookup_type}]'


class ATDBProcessingSite(models.Model):
    name = models.CharField(primary_key=True, max_length=100)
    url = models.URLField()
    access_token = models.CharField(max_length=1000, null=True)

    def __str__(self):
        return str(self.name) + " - " + str(self.url)


class SUBMISSION_STATUS(models.TextChoices):
    """Status of Work Specification"""

    # The surls and belonging information are being retrieved
    LOADING = "L", _("loading")
    # Tasks are ready to submit to ATDB
    READY = "R", _("ready")
    # Task are still being submitted to ATDB
    DEFINING = "D", _("defining")
    # Tasks are submitted, check ATDB for current status
    SUBMITTED = "S", _("submitted")
    # Check log, retry submission
    ERROR = "E", _("error")


class PURGE_POLICY(models.TextChoices):
    """ATDB Purge Policy"""

    # @Mancini, what are these?
    YES = "yes", _("yes")
    NO = "no", _("no")
    DO = "do", _("do")  # only purge, no workflow execution


class Group(models.Model):
    """Group to which work specification belongs"""

    name = models.CharField(max_length=50, unique=True)

    # ATDB info
    processing_site = models.ForeignKey(
        ATDBProcessingSite, null=True, blank=True, on_delete=models.SET_NULL
    )
    selected_workflow = models.CharField(max_length=500, null=True)
    selected_workflow_tag = models.CharField(max_length=500, null=True)

    predecessor_specification = models.ForeignKey(
        "lofardata.WorkSpecification",
        null=True,
        on_delete=models.SET_NULL,
        blank=True,
        related_name="+",   # See https://docs.djangoproject.com/en/4.1/ref/models/fields/#django.db.models.ForeignKey.related_name
    )
    filters = models.JSONField(null=True)

    task_filter = models.CharField(max_length=30, blank=True, null=True)

    def ordered_workspecifications(self):
        return self.workspecification_set.order_by('id')


class WorkSpecification(ExportModelOperationsMixin("work-specification"), models.Model):
    """Work Specification"""

    created_on = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, null=True)

    group = models.ForeignKey(Group, null=True, blank=True, on_delete=models.CASCADE)

    filters = models.JSONField(null=True)
    # Input data containing sizes/urls for submission to ATDB
    inputs = models.JSONField(null=True, blank=True)

    # ATDB Workflow URL
    selected_workflow = models.CharField(max_length=500, null=True)
    selected_workflow_tag = models.CharField(max_length=500, null=True)

    # Task ID's that were created in ATDB
    atdb_task_ids = ArrayField(models.IntegerField(), null=True, blank=True)
    predecessor_specification = models.ForeignKey(
        "self",
        null=True,
        on_delete=models.SET_NULL,
        related_name="successor",
        blank=True,
    )

    # Should automatically submit the task after defining
    is_auto_submit = models.BooleanField(default=False)

    async_task_result = models.CharField(max_length=100, null=True)
    processing_site = models.ForeignKey(
        ATDBProcessingSite, null=True, on_delete=models.DO_NOTHING
    )
    purge_policy = models.CharField(
        max_length=16, choices=PURGE_POLICY.choices, default=PURGE_POLICY.NO
    )

    task_filter = models.CharField(max_length=30, blank=True, null=True)

    __original_submission_status = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__original_submission_status = self.submission_status

    def create_successor(self):
        successor = WorkSpecification()
        successor.predecessor_specification = self
        successor.processing_site = self.processing_site
        successor.filters = self.filters

        return successor

    def created_by_display_value(self):
        if self.created_by is None:
            return "Unknown"
        return (
            self.created_by.get_full_name()
            if self.created_by.get_full_name()
            else self.created_by
        )

    def can_be_edited_by(self, user: User) -> bool:
        """
        Indicates whether this specification can be edited (i.e., updated/deleted) by the specified user
        @param user: the user to check access for
        @return: whether the specification can be updated or deleted by the specified user, which is true if:
        - the user is a staff member, OR
        - the specification has not been added to the database yet (in which case, _state == adding) OR
        - the specification was created by the user
        """
        return user.is_staff or self._state.adding or self.created_by_id == user.pk

    @staticmethod
    def can_be_viewed_by(user: User) -> bool:
        """
        Indicates whether this specification can be viewed (i.e., read) by the specified user
        @param user: the user to check access for
        @return: whether the specification can be viewed by the specified user, which is true if:
        - always
        """
        return True

    @staticmethod
    def can_be_submitted_by(user: User) -> bool:
        """
        Indicates whether this specification can be submitted by the specified user
        @param user: the user to check access for
        @return: whether the specification can be submitted by the specified user, which is true if:
        - always
        """
        return True

    @property
    def helpdesk_contact(self):
        # TODO: use API? to already fill in part of the request
        return "https://support.astron.nl/jira/servicedesk/customer/portal/7"

    def is_new(self) -> bool:
        return self.pk is None

    def is_editable(self) -> bool:
        """Indicates whether this specification is editable (i.e., it has not been submitted yet)"""
        return (
            self.submission_status != SUBMISSION_STATUS.SUBMITTED
            and self.submission_status != SUBMISSION_STATUS.DEFINING
            and self.submission_status != SUBMISSION_STATUS.LOADING
        )

    @property
    def get_file_info(self):
        """@return: a tuple of the total_size, number_of_files and average_file_size"""
        total_size, number_of_files, average_file_size = inputs_processor.compute_size_of_inputs(self.inputs)
        return total_size, number_of_files, average_file_size

    @property
    def get_file_info_dict(self):
        total_size, number_of_files, average_file_size = inputs_processor.compute_size_of_inputs(self.inputs)
        return {
            "total_size": total_size,
            "formatted_total_size": format_size(total_size),
            "average_file_size": average_file_size,
            "formatted_average_file_size": format_size(average_file_size),
            "number_of_files": number_of_files
        }

    @property
    def did_submission_status_change(self):
        return self.submission_status != self.__original_submission_status

    @property
    def should_auto_submit(self):
        is_ready = self.submission_status == SUBMISSION_STATUS.READY
        contains_files = self.get_file_info[1] > 0
        return is_ready and contains_files and self.is_auto_submit

    def __str__(self):
        return (
            str(self.id) + " - " + str(self.filters) + " (" + str(self.created_on) + ")"
        )

    # How many files per task. 0 is single task with all files
    batch_size = models.IntegerField(default=0, null=False, blank=False)
    submission_status = models.CharField(
        max_length=1,
        choices=SUBMISSION_STATUS.choices,
        default=SUBMISSION_STATUS.LOADING,
    )

    error_message = models.CharField(max_length=500, null=True, blank=True)

    def save(
        self, force_insert=False, force_update=False, using=None, update_fields=None
    ):
        super(WorkSpecification, self).save(
            force_insert=force_insert,
            force_update=force_update,
            using=using,
            update_fields=update_fields,
        )
        if self.should_auto_submit:
            from lofardata.task.tasks import insert_task_into_atdb

            insert_task_into_atdb.delay(self.pk)

        if self.async_task_result is None:
            from lofardata.task.tasks import define_work_specification

            res: AsyncResult = define_work_specification.delay(self.pk)
            self.async_task_result = res.id

            super(WorkSpecification, self).save(update_fields=["async_task_result"])
