# Generated by Django 3.1.4 on 2022-07-28 10:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lofardata', '0003_increase_filter_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dataproduct',
            name='project',
            field=models.CharField(max_length=50),
        ),
    ]
