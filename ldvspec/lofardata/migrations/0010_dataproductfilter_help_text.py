# Generated by Django 3.2 on 2022-11-11 15:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lofardata', '0009_auto_20221110_1307'),
    ]

    operations = [
        migrations.AddField(
            model_name='dataproductfilter',
            name='help_text',
            field=models.CharField(blank=True, default='', max_length=150),
        ),
    ]
