# Generated by Django 4.1.5 on 2023-01-18 15:41

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lofardata', '0015_alter_dataproduct_dysco_compression'),
    ]

    operations = [
        migrations.AlterField(
            model_name='workspecification',
            name='inputs',
            field=models.JSONField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='workspecification',
            name='related_tasks',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.IntegerField(), blank=True, null=True, size=None),
        ),
    ]
