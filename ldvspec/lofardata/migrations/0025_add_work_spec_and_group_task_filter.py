# Generated by Django 3.2 on 2023-07-25 09:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lofardata', '0024_insert_or_update_filters'),
    ]

    operations = [
        migrations.AddField(
            model_name='group',
            name='task_filter',
            field=models.CharField(blank=True, max_length=30, null=True),
        ),
        migrations.AddField(
            model_name='workspecification',
            name='task_filter',
            field=models.CharField(blank=True, max_length=30, null=True),
        ),
        migrations.AlterField(
            model_name='group',
            name='selected_workflow',
            field=models.CharField(max_length=500, null=True),
        ),
        migrations.AlterField(
            model_name='group',
            name='selected_workflow_tag',
            field=models.CharField(max_length=500, null=True),
        ),
    ]
