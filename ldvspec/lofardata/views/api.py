from django.shortcuts import render
from lofardata.models import ATDBProcessingSite

def api(request):
    atdb_hosts = ATDBProcessingSite.objects.values("name", "url")
    return render(request, "lofardata/api.html", {"atdb_hosts": atdb_hosts})