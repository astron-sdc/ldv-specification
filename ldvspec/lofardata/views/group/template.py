from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ObjectDoesNotExist
from django.views.generic import UpdateView
from rest_framework.reverse import reverse_lazy

from lofardata.forms import GroupForm
from lofardata.models import ATDBProcessingSite, Group
from lofardata.view_helpers import filters_preprocessor
from lofardata.view_helpers.specification import (
    create_work_specifications_for_group,
    split_obs_ids_string, update_work_specifications_in_group,
)


class GroupCreateUpdateView(LoginRequiredMixin, UpdateView):
    template_name = "lofardata/group/create_update.html"
    model = Group
    form_class = GroupForm

    def get_object(self, queryset=None):
        if self.kwargs.__len__() == 0 or self.kwargs["pk"] is None:
            group = Group()
        else:
            group = Group.objects.get(pk=self.kwargs["pk"])
        return group

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        processing_sites = list(ATDBProcessingSite.objects.values("name", "url"))
        context["processing_sites"] = processing_sites
        context["single_processing_site"] = processing_sites[0]['name'] if len(processing_sites) == 1 else None

        try:
            group = Group.objects.get(pk=context["object"].pk)
        except ObjectDoesNotExist:
            group = None

        # Excludes obs_id, since groups handle these differently
        context["filters"] = filters_preprocessor.preprocess(group, exclude=["obs_id"])
        return context

    def form_valid(self, form):
        group = form.save()

        if form.data.get("obs_ids"):
            self.append_work_specifications_to_group(form, group)

        update_work_specifications_in_group(group, self.request.user)

        return super().form_valid(form)



    def append_work_specifications_to_group(self, form, group):
        obs_ids = split_obs_ids_string(form.data.get("obs_ids"))
        batch_size = form.data.get("batch_size")
        create_work_specifications_for_group(group, self.request.user, obs_ids, batch_size)

    def get_success_url(self, **kwargs):
        return reverse_lazy("index")
