from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.views.generic import UpdateView, DetailView, DeleteView, ListView
from rest_framework.reverse import reverse_lazy

from lofardata.decorators import silk_profiler
from lofardata.forms import WorkSpecificationForm
from lofardata.mixins import CanEditWorkSpecificationMixin, CanViewWorkSpecificationMixin
from lofardata.models import WorkSpecification, ATDBProcessingSite, Group
from lofardata.view_helpers import filters_preprocessor, inputs_processor, dataproductinfo
from lofardata.view_helpers.group import delete_empty_groups
from lofardata.view_helpers.specification import set_post_submit_values


class WorkSpecificationCreateUpdateView(LoginRequiredMixin, CanEditWorkSpecificationMixin, UpdateView):
    template_name = "lofardata/workspecification/create_update.html"
    model = WorkSpecification
    form_class = WorkSpecificationForm

    @silk_profiler(name="Create/update work specification")
    def get(self, request, *args, **kwargs):
        specification = self.get_object()
        if specification.is_new() or specification.is_editable():
            return super().get(request, *args, **kwargs)
        else:
            return redirect('specification-detail', self.kwargs["pk"])

    def get_object(self, queryset=None):
        if self.kwargs.__len__() == 0 or self.kwargs["pk"] is None:
            specification = WorkSpecification()
        else:
            specification = WorkSpecification.objects.get(pk=self.kwargs["pk"])
        return specification

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["filters"] = filters_preprocessor.preprocess(context["workspecification"])
        processing_sites = list(ATDBProcessingSite.objects.values("name", "url"))
        context["processing_sites"] = processing_sites
        context["single_processing_site"] = processing_sites[0]['name'] if len(processing_sites) == 1 else None
        context["groups"] = list(Group.objects.values("name", "id", "processing_site", "selected_workflow", "selected_workflow_tag", "task_filter"))
        return context

    def create_successor(self, specification):
        successor = specification.create_successor()
        successor.save()
        return self.get_success_url(pk=successor.pk)

    def form_valid(self, form):
        action_ = form.data["action"]
        specification = form.instance

        set_post_submit_values(specification, self.request.user)

        if action_ == "Successor":
            specification.save()
            return HttpResponseRedirect(self.create_successor(specification))

        return super().form_valid(form)

    def get_success_url(self, **kwargs):
        if kwargs.__len__() == 0 or kwargs["pk"] is None:
            return reverse_lazy("index")
        else:
            return reverse_lazy("specification-update", kwargs={"pk": kwargs["pk"]})

class WorkSpecificationDetailView(LoginRequiredMixin, CanViewWorkSpecificationMixin, DetailView):
    template_name = "lofardata/workspecification/detail.html"
    model = WorkSpecification

    @silk_profiler(name="View work specification")
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        specification = WorkSpecification.objects.get(pk=context["object"].pk)
        context["filters"] = filters_preprocessor.preprocess(specification)
        total_input_size, number_of_files, average_file_size = inputs_processor.compute_size_of_inputs(
            specification.inputs
        )
        context["number_of_files"] = number_of_files
        context["number_of_unspecified_files"] = dataproductinfo.count_unspecified_files(specification.id)
        context["total_input_size"] = inputs_processor.format_size(total_input_size)
        context["size_per_task"] = inputs_processor.format_size(
            average_file_size * specification.batch_size
            if specification.batch_size > 0
            else total_input_size
        )
        return context

class WorkSpecificationDeleteView(LoginRequiredMixin, CanEditWorkSpecificationMixin, DeleteView):
    template_name = "lofardata/workspecification/delete.html"
    model = WorkSpecification
    success_url = reverse_lazy("index")

    def delete(self, request, *args, **kwargs):
        specification = self.get_object()
        specification.delete()

        delete_empty_groups()

        return HttpResponseRedirect(self.success_url)

class WorkSpecificationInputsView(LoginRequiredMixin, CanViewWorkSpecificationMixin, DetailView):
    template_name = "lofardata/workspecification/inputs.html"
    model = WorkSpecification

class WorkSpecificationATDBTasksView(LoginRequiredMixin, CanViewWorkSpecificationMixin, DetailView):
    template_name = "lofardata/workspecification/tasks.html"
    model = WorkSpecification

class WorkSpecificationDatasetSizeInfoView(LoginRequiredMixin, CanViewWorkSpecificationMixin, DetailView):
    template_name = "lofardata/workspecification/dataset_size_info.html"
    model = WorkSpecification

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        specification = WorkSpecification.objects.get(pk=context["object"].pk)

        min_size, max_size, n_bins, counts, biggest_bucket, bins = inputs_processor.compute_inputs_histogram(
            specification.inputs)

        context["min_size"] = inputs_processor.format_size(min_size)
        context["max_size"] = inputs_processor.format_size(max_size)
        context["biggest_bucket"] = biggest_bucket
        context["n_bins"] = n_bins
        context["counts"] = counts
        context["bins"] = bins

        return context

class Overview(ListView):
    model = Group
    ordering = ["id"]
    template_name = "lofardata/index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["ungrouped_work_specifications"] = WorkSpecification.objects.filter(group=None).order_by('id')
        return context
