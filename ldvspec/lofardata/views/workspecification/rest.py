from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponseNotFound, HttpResponseForbidden, HttpResponse
from django.shortcuts import redirect
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from lofardata.filters import get_submittable_workspecifications
from lofardata.models import WorkSpecification
from lofardata.serializers import WorkSpecificationSerializer, EnrichedWorkSpecificationSerializer
from lofardata.view_helpers.group import delete_empty_groups
from lofardata.view_helpers.submit import submit_single_to_atdb, submit_list_to_atdb


class WorkSpecificationViewset(viewsets.ModelViewSet):
    queryset = WorkSpecification.objects.all().order_by('id')
    serializer_class = WorkSpecificationSerializer
    permission_classes = [IsAuthenticated]

    @action(methods=['GET'], detail=False)
    def enriched(self, request):
        serializer = EnrichedWorkSpecificationSerializer(self.queryset.all(), many=True)
        return Response(serializer.data)

    @action( methods=['POST'], detail=True)
    def submit(self, request, pk=None) -> HttpResponse:
        try:
            workspecification = WorkSpecification.objects.get(pk=pk)
        except ObjectDoesNotExist:
            return HttpResponseNotFound()

        if not workspecification.can_be_submitted_by(request.user):
            return HttpResponseForbidden()

        submit_single_to_atdb(pk)

        return redirect('index')

    @action(url_path='submit-multiple', methods=['POST'], detail=False)
    def submit_multiple(self, request) -> HttpResponse:
        submitted_ids = request.data.get('ids', [])

        submittable_workspecifications = get_submittable_workspecifications(WorkSpecification.objects.filter(id__in=submitted_ids))

        actual_ids = [x.id for x in submittable_workspecifications if x.can_be_submitted_by(request.user)]

        submit_list_to_atdb(actual_ids)

        return Response(data=actual_ids)

    @action(url_path='delete-multiple', methods=['POST'], detail=False)
    def delete_multiple(self, request) -> HttpResponse:
        submitted_ids = request.data.get('ids', [])

        actual_workspecifications = WorkSpecification.objects.filter(id__in=submitted_ids)

        deletable_workspecificatios = [work_spec for work_spec in actual_workspecifications if work_spec.can_be_edited_by(request.user)]
        deleted_ids = [work_spec.pk for work_spec in deletable_workspecificatios]

        for workspecification in deletable_workspecificatios:
            workspecification.delete()

        delete_empty_groups()

        return Response(data=deleted_ids)