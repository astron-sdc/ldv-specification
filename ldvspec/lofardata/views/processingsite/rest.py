from rest_framework import viewsets
from lofardata.models import ATDBProcessingSite
from lofardata.serializers import ATDBProcessingSiteSerializer

class ATDBProcessingSiteView(viewsets.ReadOnlyModelViewSet):
    model = ATDBProcessingSite
    serializer_class = ATDBProcessingSiteSerializer
    queryset = ATDBProcessingSite.objects.all().order_by("pk")