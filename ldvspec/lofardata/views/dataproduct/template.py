from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ObjectDoesNotExist
from django.views.generic import TemplateView

from lofardata.mixins import CanViewWorkSpecificationMixin
from lofardata.models import WorkSpecification
from lofardata.view_helpers import dataproductinfo


class DataProductViewPerSasID(LoginRequiredMixin, CanViewWorkSpecificationMixin, TemplateView):
    template_name = "lofardata/workspecification/dataproducts.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        try:
            specification = self.get_object()
        except ObjectDoesNotExist:
            return context

        sas_id = specification.filters['obs_id']
        context["sas_id"] = sas_id
        context["dataproduct_info"] = dataproductinfo.retrieve_combined_information(sas_id)
        return context

    def get_object(self):
        return WorkSpecification.objects.get(pk=self.kwargs['pk'])