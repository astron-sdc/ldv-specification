from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from lofardata.filters import DataProductFilterSet
from lofardata.models import DataProduct
from lofardata.serializers import DataProductSerializer, DataProductFlatSerializer
from django_filters import rest_framework as filters
from rest_framework.schemas.openapi import AutoSchema

class DataProductView(generics.ListCreateAPIView):
    model = DataProduct
    serializer_class = DataProductSerializer

    queryset = DataProduct.objects.all().order_by("obs_id")

    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = DataProductFilterSet

    permission_classes = [IsAuthenticated]

class DataProductDetailsView(generics.RetrieveUpdateDestroyAPIView):
    model = DataProduct
    serializer_class = DataProductSerializer
    queryset = DataProduct.objects.all()

class InsertWorkSpecificationSchema(AutoSchema):
    def get_operation_id_base(self, path, method, action):
        return "createDataProductMulti"

class InsertMultiDataproductView(generics.CreateAPIView):
    """
    Add single DataProduct
    """

    queryset = DataProduct.objects.all()
    serializer_class = DataProductFlatSerializer
    schema = InsertWorkSpecificationSchema()

    def get_serializer(self, *args, **kwargs):
        """if an array is passed, set serializer to many"""
        if isinstance(kwargs.get("data", {}), list):
            kwargs["many"] = True
        return super().get_serializer(*args, **kwargs)