from django.contrib import admin

# Register your models here.
from .models import DataProduct, DataProductFilter, ATDBProcessingSite, WorkSpecification, Group

admin.site.register(DataProduct)
admin.site.register(DataProductFilter)
admin.site.register(ATDBProcessingSite)
admin.site.register(WorkSpecification)
admin.site.register(Group)