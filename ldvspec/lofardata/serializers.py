from rest_framework import serializers
from .models import DataProduct, WorkSpecification, ATDBProcessingSite, Group


class ATDBProcessingSiteSerializer(serializers.ModelSerializer):
    class Meta:
        model = ATDBProcessingSite
        fields = '__all__'


class DataProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = DataProduct
        fields = "__all__"


def validate_location(attrs):
    valid_location_uris = ['srm.grid.sara.nl',
                           'lta-head.lofar.psnc.pl',
                           'lofar-srm.fz-juelich.de',
                           'srm.target.rug.nl']

    if attrs['location'] not in valid_location_uris:
        attrs['location'] = 'Unknown'

    return attrs


class DataProductFlatSerializer(serializers.ModelSerializer):
    class Meta:
        model = DataProduct
        fields = '__all__'

    def validate(self, attrs):
        return validate_location(attrs)

    def create(self, validated_data):
        return DataProduct.insert_dataproduct(**validated_data)


class WorkSpecificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = WorkSpecification
        fields = '__all__'

    def create(self, validated_data):
        validated_data['created_by'] = self.context['request'].user
        instance = super().create(validated_data)
        return instance

class EnrichedWorkSpecificationSerializer(serializers.ModelSerializer):
    file_info = serializers.ReadOnlyField(source="get_file_info_dict")
    is_editable = serializers.ReadOnlyField()

    class Meta:
        model = WorkSpecification
        exclude=('inputs', )

class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = '__all__'