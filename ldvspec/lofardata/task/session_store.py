import requests


class SessionStore:
    """requests.Session Singleton"""

    _session = None

    @classmethod
    def get_session(cls) -> requests.Session:
        if cls._session is None:
            cls._session = requests.Session()
        return cls._session
