import re
from typing import Any, Dict, List, Optional, Union
from urllib.parse import urlparse

from django.db.models import Q, QuerySet

from lofardata.models import (
    SUBMISSION_STATUS,
    ATDBProcessingSite,
    WorkSpecification, DataProduct, DataProductFilter, DataFilterInclusionType, DataFilterCardinalityType,
)

from lofardata.task.token_auth import TokenAuth

from lofardata.task.custom_exceptions import RequestNotOk, InvalidPredecessor, InvalidLocation, WorkSpecificationNoSite, \
    WorkerResponseNotOk, TaskException, ATDBKaput, InvalidSurl


def parse_surl(surl: str, work_spec: WorkSpecification) -> dict:
    parsed = urlparse(surl)
    host = parsed.hostname
    path = parsed.path
    pattern = r"^.*/projects\/(?P<project>.+)\/(?P<sas_id>\w\d*)\/"

    try:
        data = re.match(pattern, path).groupdict()
    except Exception as error:
        set_errored_work_specification(work_spec, InvalidSurl, error)
        raise InvalidSurl

    valid_locations = ['srm.grid.sara.nl', 'lofar-srm.fz-juelich.de', 'lta-head.lofar.psnc.pl']
    if host in valid_locations:
        data["location"] = host
        return data
    else:
        set_errored_work_specification(work_spec, InvalidLocation)
        raise InvalidLocation('location= {}'.format(host))


def create_atdb_inputs_file(entries):

    inputs = [
        {
            "size": entry["size"],
            "surl": entry["surl"],
            "type": "File",
            "location": entry["surl"].split('://')[1].split(':')[0],
        }
        for entry in entries
    ]
    return inputs


def split_entries_to_batches(surls: List[Any], batch_size: int) -> List[List[Any]]:
    """Split the list of entries into batches of at most `batch_size` entries"""
    if batch_size == 0:
        raise RequestNotOk()

    n_surls = len(surls)

    # NOTE: Think about using file size instead of amount of files
    batches: List[List[Any]] = []
    num_batches = n_surls // batch_size
    num_batches += 1 if n_surls % batch_size else 0

    for n in range(num_batches):
        batches.append(surls[n * batch_size: (n + 1) * batch_size])

    return batches


def send_to_atdb(payload, sess, site, work_spec):
    response = sess.post(site.url + "tasks/", json=payload, auth=TokenAuth(site.access_token))
    if not response.ok:
        set_errored_work_specification(work_spec, WorkerResponseNotOk, None)
        raise WorkerResponseNotOk

    if work_spec.atdb_task_ids is None:
        work_spec.atdb_task_ids = []

    try:
        response_task_id = response.json()["id"]
        work_spec.atdb_task_ids.append(response_task_id)
    except Exception as error:
        set_errored_work_specification(work_spec, ATDBKaput, error)
        raise ATDBKaput


def create_payload(batch, work_spec: WorkSpecification, atdb_predecessor_task_id=None) -> dict:
    # Parse a single surl for info:
    # project, sas_id
    if len(batch) < 1:
        set_errored_work_specification(work_spec, RequestNotOk)
        raise RequestNotOk('Cannot create a payload for an empty batch')

    parsed = parse_surl(batch[0]["surl"], work_spec)
    project_id = parsed["project"]
    sas_id = parsed["sas_id"]

    payload = {
        "project": project_id,
        "sas_id": sas_id,
        "task_type": "regular",
        "filter": work_spec.task_filter if work_spec.task_filter and work_spec.task_filter.strip() else f"ldv-spec:{work_spec.pk}",
        "purge_policy": work_spec.purge_policy,
        "new_status": "defining",
        "new_workflow_uri": work_spec.selected_workflow,
        "size_to_process": sum([e["size"] for e in batch]),
        "inputs": (create_atdb_inputs_file(batch)),
    }

    if atdb_predecessor_task_id:
        payload["predecessor"] = atdb_predecessor_task_id

    return payload


def get_atdb_predecessor_task_id(work_spec):
    # Task ID of the predecessor
    atdb_predecessor_task_id: Union[int, None] = None
    if work_spec.predecessor_specification is not None:
        predecessor: WorkSpecification = work_spec.predecessor_specification
        # Should only be 1 entry
        if predecessor.atdb_task_ids is None or len(predecessor.atdb_task_ids) != 1:
            raise InvalidPredecessor()
        atdb_predecessor_task_id = predecessor.atdb_task_ids[0]
    return atdb_predecessor_task_id


def get_processing_site(work_spec):
    site: Optional[ATDBProcessingSite] = work_spec.processing_site
    if site is None:
        set_errored_work_specification(work_spec, WorkSpecificationNoSite, None)
        raise WorkSpecificationNoSite
    return site


def get_surls(work_spec) -> List[dict]:
    if work_spec.inputs is None:
        set_errored_work_specification(work_spec, RequestNotOk)
        raise RequestNotOk("There is no 'inputs' for this work specification with id: {}".format(work_spec.id))

    inputs: Dict[str, Any] = work_spec.inputs.copy()
    try:
        entries: List[dict] = inputs.pop("surls")
        if len(entries) < 1:
            raise RequestNotOk("There are no surls known for this work specification with id: {}".format(work_spec.id))
        return entries
    except Exception as err:
        set_errored_work_specification(work_spec, RequestNotOk, err)
        raise RequestNotOk


def set_errored_work_specification(work_spec: WorkSpecification, exception: TaskException, error: BaseException = None):
    work_spec.submission_status = SUBMISSION_STATUS.ERROR
    work_spec.error_message = exception.default_detail + "\n" + str(error)[:len(exception.default_detail) - 1]
    work_spec.save()

def get_dataproducts_by_filters(filters: dict, filter_definitions: Optional[List[Optional[DataProductFilter]]]) -> QuerySet[DataProduct]:
    if not filters:
        raise ValueError('filters is not defined')

    query = Q()

    for key, value in filters.items():
        definition: DataProductFilter # annotate before unpacking: https://peps.python.org/pep-0526/#global-and-local-variable-annotations
        [definition] = [definition for definition in filter_definitions if definition.field == key] if filter_definitions else [None]

        lookup_type = definition.lookup_type if definition else 'exact'
        inclusion_type = definition.filter_inclusion_type if definition else DataFilterInclusionType.INCLUDE
        cardinality_type = definition.filter_cardinality_type if definition else DataFilterCardinalityType.SINGLE

        query = add_to_query(
            query=query,
            key=key,
            value=value,
            lookup_type=lookup_type,
            inclusion_type=inclusion_type,
            cardinality_type=cardinality_type)

    dataproducts = DataProduct.objects.filter(query)

    return dataproducts

def add_to_query(query: Q, key: str, value: str, lookup_type: str, inclusion_type: DataFilterInclusionType, cardinality_type: DataFilterCardinalityType) -> Q:
    query_part = Q()

    values = []

    if cardinality_type == DataFilterCardinalityType.SINGLE:
        values = [value]
    elif cardinality_type == DataFilterCardinalityType.MULTIPLE:
        values = value.splitlines()

    for value in values:
        sub_query_part = Q(**{f'{key}__{lookup_type}': value})

        if inclusion_type == DataFilterInclusionType.EXCLUDE:
            sub_query_part = ~sub_query_part

        query_part.add(sub_query_part, Q.AND)

    query.add(query_part, Q.AND)

    return query