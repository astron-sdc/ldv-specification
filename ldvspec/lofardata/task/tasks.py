from lofardata.models import (
    SUBMISSION_STATUS,
    DataProduct,
    WorkSpecification, DataProductFilter,
)

from ldvspec.celery import app

from lofardata.task.session_store import SessionStore
from lofardata.task.task_helpers import get_processing_site, get_surls, split_entries_to_batches, \
    get_atdb_predecessor_task_id, create_payload, send_to_atdb, set_errored_work_specification, \
    get_dataproducts_by_filters

from lofardata.task.custom_exceptions import UnknownError, TaskException


@app.task
def define_work_specification(workspecification_id):
    specification = WorkSpecification.objects.get(pk=workspecification_id)

    filters = specification.filters
    filter_definitions = list(DataProductFilter.objects.filter(field__in=filters))

    dataproducts = \
        get_dataproducts_by_filters(filters, filter_definitions).filter(filesize__gt=0).order_by("surl") if filters else []

    inputs = {
        "surls": [
            {"surl": dataproduct.surl, "size": dataproduct.filesize}
            for dataproduct in dataproducts
        ]
    }
    if specification.inputs is None:
        specification.inputs = inputs
    else:
        specification.inputs.update(inputs)
    specification.submission_status = SUBMISSION_STATUS.READY
    specification.save()


@app.task
def insert_task_into_atdb(workspecification_id: int):
    """This creates the task in ATDB and set's it to defining"""
    sess = SessionStore.get_session()

    work_spec: WorkSpecification = WorkSpecification.objects.get(
        pk=workspecification_id
    )

    work_spec.submission_status = SUBMISSION_STATUS.DEFINING
    work_spec.save()

    try:
        site = get_processing_site(work_spec)
        surls = get_surls(work_spec)

        if work_spec.batch_size > 0:
            batches = split_entries_to_batches(surls, work_spec.batch_size)
        else:
            batches = [surls]

        atdb_predecessor_task_id = get_atdb_predecessor_task_id(work_spec)

        for batch in batches:
            payload = create_payload(batch, work_spec, atdb_predecessor_task_id)
            send_to_atdb(payload, sess, site, work_spec)

        work_spec.submission_status = SUBMISSION_STATUS.SUBMITTED
        work_spec.error_message = None
        work_spec.save()
    except TaskException as task_error:
        raise task_error
    except Exception as err:  # When this happens, fix it in the task_helpers class (+ add the test)
        set_errored_work_specification(work_spec, UnknownError, err)
