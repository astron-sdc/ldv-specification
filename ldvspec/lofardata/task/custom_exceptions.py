class TaskException(BaseException):
    default_detail = ""
    default_code = ""


class UnknownError(TaskException):  # When this happens, fix it in the task_helpers class (+ add the test)
    default_detail = 'Something has gone wrong.'
    default_code = 'Bad request'


class RequestNotOk(TaskException):
    default_detail = 'You are trying to submit a work specification with invalid data.'
    default_code = 'Unprocessable Entity: invalid data'


class WorkerResponseNotOk(TaskException):
    default_detail = 'ATDB wil not handle this request.'
    default_code = 'I am a teapot: ATDB error response'''


class WorkSpecificationNoSite(TaskException):
    default_detail = 'You are trying to submit a work specification without a target processing site. We cannot send it into outer space.'
    default_code = 'Bad request: no processing site'


class InvalidPredecessor(TaskException):
    default_detail = 'You are trying to submit a work specification where the predecessor is faulty or no longer exists.'
    default_code = 'Unprocessable Entity: invalid predecessor'


class InvalidLocation(TaskException):
    default_detail = 'You are trying to submit a work specification where the location is not Sara, Jeulich or Poznan. This should never happen so there is something wrong with the used LTA data.'
    default_code = 'Unprocessable Entity: invalid processing location'


class ATDBKaput(TaskException):
    default_detail = "ATDB didn't give a task response while this should have happened. This is a problem with ATDB and not LDV spec."
    default_code = 'I am a teapot: ATDB kaput'''


class InvalidSurl(TaskException):
    default_detail = 'You are trying to submit a work specification with an invalid surl. It should at least consist of a project, a sas_id and a location.'
    default_code = 'Unprocessable Entity: invalid data'
