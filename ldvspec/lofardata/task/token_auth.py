import requests
from requests.auth import AuthBase


class TokenAuth(AuthBase):
    """Basic Token Auth

    Adds a: `Authorization: Token <token>` header"""

    def __init__(self, token: str):
        self._token = token

    def __call__(self, r: requests.PreparedRequest) -> requests.PreparedRequest:
        r.headers["Authorization"] = f"Token {self._token}"
        return r