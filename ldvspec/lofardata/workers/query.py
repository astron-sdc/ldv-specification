from uws.classes import UWSJob
from uws.client import Client
from uws.workers import Worker
from lofardata.models import DataProduct, WorkSpecification


class Echo(Worker):
    """A worker echoing all parameters"""

    def __init__(self):
        self._type = "echo"

    def run(self, job: UWSJob, job_token: str, client: Client) -> None:
        data = [{"key": p.key, "value": p.value} for p in job.parameters]
        client.add_results(job.jobId, data, job_token)


class PrepareWorkSpecification(Worker):

    def __init__(self):
        self._type = "query_dataproducts"

    def run(self, job: UWSJob, job_token: str, client: Client) -> None:
        pass
        # parameters = {p.key: p.value for p in job.parameters}
        # specification_id = parameters.pop('specification_id')
        work_specification = WorkSpecification.objects.get(pk=specification_id)

        # dataproducts = DataProduct.objects.filter(**filters)
        # for data in data
        # client.add_results(job.jobId, [], job_token)
