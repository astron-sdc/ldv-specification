from django.contrib.auth.mixins import UserPassesTestMixin

class CanEditWorkSpecificationMixin(UserPassesTestMixin):
    """
    A mixin for checking whether a work specification can be edited (updated/deleted) by a particular user
    Works on all views that implement get_object (DetailView etc)
    """
    def test_func(self, **kwargs):
        return self.get_object().can_be_edited_by(self.request.user)

class CanViewWorkSpecificationMixin(UserPassesTestMixin):
    """
    A mixin for checking whether a work specification can be viewed by a particular user
    Works on all views that implement get_object (DetailView etc)
    """
    def test_func(self, **kwargs):
        return self.get_object().can_be_viewed_by(self.request.user)

class CanSubmitWorkSpecificationMixin(UserPassesTestMixin):
    """
    A mixin for checking whether a work specification can be submitted by a particular user
    Works on all views that implement get_object (DetailView etc)
    """

    def test_func(self, **kwargs):
        return self.get_object().can_be_submitted_by(self.request.user)