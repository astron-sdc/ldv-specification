from typing import List

from django.db.models import QuerySet
from django_filters import rest_framework as filters

from .models import (
    DataProduct,
    DataProductFilter, SUBMISSION_STATUS, WorkSpecification,
)

class DynamicFilterSet(filters.FilterSet):
    class Meta:
        filter_class = None

    def __init__(self, *args, **kwargs):
        self._load_filters()
        super().__init__(*args, **kwargs)

    def _load_filters(self):
        if self.Meta.filter_class is None:
            raise Exception("Define filter_class meta attribute")
        for item in self.Meta.filter_class.objects.all():
            field_obj = self.Meta.model._meta.get_field(item.field)
            filter_class, *_ = self.filter_for_lookup(field_obj, item.lookup_type)
            self.base_filters[item.name] = filter_class(item.field)

class DataProductFilterSet(DynamicFilterSet):
    class Meta:
        model = DataProduct
        filter_class = DataProductFilter
        fields = {
            "obs_id": ["exact", "icontains"],
        }

def get_submittable_workspecifications(workspecfications: QuerySet) -> List[WorkSpecification]:
    workspecifications_with_surls = workspecfications.filter(inputs__surls__0__isnull=False)
    workspecifications_with_submittable_status = workspecifications_with_surls.filter(
        submission_status__in=[SUBMISSION_STATUS.READY, SUBMISSION_STATUS.ERROR])
    workspecifications_with_surl_data = [x for x in workspecifications_with_submittable_status if x.get_file_info[0] != 0]
    return workspecifications_with_surl_data