// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'LDV Specification',
  tagline: 'Guides and Documentation',

  // Set the production url of your site here
  url: 'https://sdc.astron.nl',
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: '/ldvspec/static/documentation/',


  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          //editUrl: 'https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          //editUrl: 'https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      // Replace with your project's social card
      image: 'img/docusaurus-social-card.jpg',
      navbar: {
        title: 'LDV Specification',
        logo: {
          alt: 'My Site Logo',
          src: '../lofardata/styling/images/ldvspec_logo_2.png',
        },
        items: [
          {
            type: 'doc',
            docId: 'user-guide/intro',
            position: 'left',
            label: 'User Guide',
          },
          // {
          //   type: 'doc',
          //   docId: 'developer-guide/intro',
          //   position: 'left',
          //   label: 'Developer Guide',
          // },
          {
            href: 'https://git.astron.nl/astron-sdc/ldv-specification',
            label: 'GitLab',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Docs',
            items: [
              {
                label: 'User Guide',
                to: '/docs/user-guide/intro',
              },
              // {
              //   label: 'Developer Guide',
              //   to: '/docs/developer-guide/intro',
              // },
            ],
          },
          {
            title: 'Community',
            items: [
              {
                label: 'Confluence',
                href: 'https://support.astron.nl/confluence/display/SDCP/LDV-Specification',
              },
            ],
          },
          {
            title: 'More',
            items: [
              {
                label: 'GitLab',
                href: 'https://git.astron.nl/astron-sdc/ldv-specification',
              },
            ],
          },
        ],
        copyright: ' ',
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
