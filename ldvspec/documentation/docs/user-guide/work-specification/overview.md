---
sidebar_position: 2
---

# The Overview Page

The work specification overview is the landing page of the application and shows a list of all work specifications created by all users. They are presented in one of two ways:

* Ungrouped. These are all work specifications that do not belong to a particular group, and appear in a group named 'Ungrouped'. You can still perform group actions on them, such as deleting and submitting.
* Grouped. These are work specifications that belong to a group. Groups appear on the overview with their name, and as a collapsible box:


![An example group on the overview page](/img/overview_group.png)

For more information on how to define and work with groups, see the [Section on groups](../group/intro)

Every work specification is presented as a row, with a number of columns and actions that can be performed (these actions are explained on their respective pages in this guide). Column values that are subject to change by submitting the specification, will automatically refresh on the screen. Some of these column values can be clicked:

* Clicking the ID value in the ID column will navigate you to the details of that work specifications
* Clicking the file icon in the "Data Product Info" column will show you a popup that displays all data product information.