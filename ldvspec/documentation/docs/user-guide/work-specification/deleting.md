---
sidebar_position: 5
---

# Deleting a Work Specification

An existing work specification can be deleted in one of two ways:

- By clicking the delete button for an individual work specification.

![The "Delete this work specification" button](/img/delete_workspecification.png) 

You will be prompted with a confirmation message. If you choose to delete a specification that has been sent to ATDB, it will not be deleted there.
- By selecting one or more work specifications by checking the checkbox that appears in front of every row, and subsequently clicking the 'delete' button at the top of the group: 

![The "Delete the selected items" button](/img/batch_delete_workspecification.png) 

It is also possible to select/deselect all specifications, with the checkbox situated the top of the group. 

The system will attempt to delete all selected items, but those items that cannot be deleted (due to reasons listed below) will be skipped.

Please note that if you delete all work specifications in a group, that group will also be deleted automatically. Also note that a work specification cannot be deleted in the following situations:

* The current user trying to delete is not the user that created the work specification, or is not marked as 'staff' user.
* The work specification has one or more successors. These should be deleted first.