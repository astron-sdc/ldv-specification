---
sidebar_position: 1
---

# Introduction

A work specification captures the information needed to process tasks on a processing site (e.g., SURF, Jülich, Poznań). The specification can be submitted to [ATDB-LDV](https://support.astron.nl/confluence/display/SDCP/LDV+Documentation). 

This section of the guide explains how work specifications are listed, and how they can be created, edited, deleted, and submitted. Additionally, there is a section that explains all the states in which a work specification can be.