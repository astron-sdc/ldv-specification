---
sidebar_position: 3
---

# Creating a Work Specification

A work specification can be created by pressing the "Create a new work specification" button at the top of the page:

![The "Create a new work specification" button](/img/create_workspecification.png)

After pressing the button, you will be presented with a popup which prompts you to fill in some information:

* An existing group. Selecting a group will populate properties with the corresponding properties of that group. Deselecting that group will clear these properties. Please refer to the [Section on groups](../group/intro) for information on groups.
* The location of ATDB-LDV (e.g., https://sdc-dev.astron.nl:5554/atdb/)
* The pipeline's workflow tag (the possible values come from ATDB, they are all the unique `description` property values across all workflows, e.g. 'test' or 'production')
* The pipeline (workflow) to run (the possible values come from ATDB)
* The related predecessor 
* The number of files every generated task should have
* The task filter, which will populate the 'filter' field in ATDB
* Auto submit: if this box is checked, the work specification will be submitted as soon as it is created

Additionally, there are a number of dataproduct filters that can you populate:

* The SAS ID
* The project code (case-insensitive)
* The activity (a list of possible choices, e.g. 'Pulsar Pipeline' or 'Raw Observation')
* The dataproduct type (a list of possible choices, e.g. 'Beam Formed Data')
* The location URI (e.g., Juelich or Poznan)
* A list of (partial) SURLS to exclude from the input selection, separated by newlines

After creating the work specification, it will appear in the work specification overview, either as part of a group or ungrouped.