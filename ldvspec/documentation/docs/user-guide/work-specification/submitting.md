---
sidebar_position: 6
---

# Submitting a Work Specification

An existing work specification can be submitted in one of two ways:

- By clicking the submit button for an individual work specification.

![The "Submit this work specification" button](/img/submit_workspecification.png) 

- By selecting one or more work specifications by checking the checkbox that appears in front of every row, and subsequently clicking the 'submit' button at the top of the group:

![The "Submit the selected items" button](/img/batch_submit_workspecification.png) 

It is also possible to select/deselect all specifications, with the checkbox situated the top of the group. 

The system will attempt to submit all selected items, but those items that cannot be submitted (due to reasons listed below) will be skipped.

* It has been created, but necessary information is still being retrieved. A 'loading' icon depicting an hourglass will appear, in conjunction with the submit icon. Please refer to the "Work Specification States" page for more information on the different states that a work specification can be in.
* It has already been submitted to ATDB.
* There are no files available for the specified filters.
