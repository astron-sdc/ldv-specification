---
sidebar_position: 4
---

# Editing a Work Specification

A work specification can be edited by pressing the "Edit this work specification" button:

![The "Edit this work specification" button](/img/edit_workspecification.png)

After pressing this icon, you will be presented with a popup which allows you to change the properties of the work specifications as described on the [Creating a Work Specification](creating) page.

Please note that a work specification cannot be edited in the following situations:

* It has been created, but necessary information is still being retrieved. A 'loading' icon depicting an hourglass will appear, in conjunction with the edit icon. Please refer to the [Work Specification States page](states) for more information on the different states that a work specification can be in.
* It has already been submitted to ATDB.
* The current user trying to edit is not the user that created the work specification, or is not marked as 'staff' user.