---
sidebar_position: 7
---

# Work Specification States

A work specification can be in one of several states. The state of a workspecification is reflected in the 'Status' badge on the work specification overview:

![The status of a work specification](/img/state_in_overview.png)


The following state diagram depicts how a work specification transitions from one state to another:

![State diagram depicting transitions](/img/state_diagram.png)

Regardless of state, a work specification will exist indefinitely until it is deleted manually. Please note that deleting a specification will not cause it to be deleted in ATDB.