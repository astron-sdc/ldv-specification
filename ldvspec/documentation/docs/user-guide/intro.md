---
displayed_sidebar: userGuideSideBar
sidebar_position: 1
---

# User Guide Introduction

Welcome to the LDV Specification Tool User Guide. This guide describes the functionality that this application offers and how can it can be used. In the menu on the left, you will find various topics that you can explore. If you have any questions, remarks, or bug reports, please use the SDC Helpdesk.