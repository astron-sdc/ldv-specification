---
sidebar_position: 2
---

# Account

To use the LDV Specification Tool, you need an account. If you don't have one, please contact the SDC Helpdesk to acquire an account. An account will be setup for you on both the test and production environment.