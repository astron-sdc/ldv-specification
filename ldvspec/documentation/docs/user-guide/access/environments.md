---
sidebar_position: 1.5
---

# Environments

There are currently two environments available for LDV Specification:

- The test environment, which can be reached at https://sdc-dev.astron.nl/ldvspec
- The production environment, which can be reached at https://sdc.astron.nl/ldvspec

As a user, you will always want to use the production environment, unless instructed otherwise. Even though we strive to keep both environments stable, the test environment may be used for testing purposes which could lead to instability on that environment. Additionally, the test environment only contains a subset of the data on the production environment.