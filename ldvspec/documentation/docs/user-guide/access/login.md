---
sidebar_position: 2
---

# Logging In

Once you have an SDC Keycloak account, click the "Sign in" button on the top right of the page. You will be redirected to log in with Keycloak automatically.
