---
sidebar_position: 2
---

# Creating a Group

A group can be created by pressing the "Create a new group" button:

![The "Create a new group" button](/img/create_group.png)

After pressing the button, you will be presented with a popup which prompts you to fill in some information:

* A name for the group, which uniquely identifies the group. This is the name that will appear on the work specification overview page. You are free to choose a name that makes sense to you.
* The processing site (e.g., ATDB) that will be assigned to all work specifications in this group.
* The workflow tag that will be assigned to all work specifications in this group.
* The selected workflow that will be assigned to all work specifications in this group.
* A comma-separated list of SAD IDs to append. For every single SAD ID in this list, a work specification is created. For example, the input `58991, 64273, 86577` would produce three separate work specifications.
* The number of files per task that will be assigned to all work specifications in this group.
* The task filter that will be assigned to all work specifications in this group.
* All filters which are also available for creating work specifications (e.g., project code, activity, type, location, excluded SURLS)

After creating a group, it will appear in the work specification overview.