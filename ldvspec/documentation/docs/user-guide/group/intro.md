---
sidebar_position: 1
---

# Introduction

A group is a collection of work specifications. They are meant to be used as a convenience for creating, deleting, and submitting multiple work specifications with a single action. All group related functionality can be found on the work specification overview page. 