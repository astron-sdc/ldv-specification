---
sidebar_position: 4
---

# Deleting a Group

A group will automatically be deleted when all work specifications in that group are deleted. When a group is deleted, it will disappear from the work specification overview page.