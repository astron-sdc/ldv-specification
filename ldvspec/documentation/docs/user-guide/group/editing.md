---
sidebar_position: 3
---

# Editing a Group

An existing group can be edited by pressing the "Edit this group" icon:

![The "Edit this group" button](/img/edit_group.png)

After pressing this icon, you will be presented with a popup which allows you to change some properties of the group:

* A name for the group, which uniquely identifies the group. You can change to the name to some other value, provided that it is unique.

For the following properties and dataproduct filters, a change will propagate to all underlying work specifications.

* The location of ATDB-LDV (e.g., https://sdc-dev.astron.nl:5554/atdb/)
* The pipeline's workflow tag (the possible values come from ATDB, they are all the unique `description` property values across all workflows, e.g. 'test' or 'production')
* The pipeline (workflow) to run (the possible values come from ATDB)
* The related predecessor
* All filters which are also available for creating work specifications (e.g., project code, activity, type, location, excluded SURLS)

It is also possible to append specifications to an existing group, by specifying the following:

* A comma-separated list of SAD IDs to append. For every single SAD ID in this list, a work specification is created. For example, the input `58991, 64273, 86577` would produce three separate work specifications.
* The number of files every generated task should have

Please note that when appending, these two changes (SAS ids and number of files) will NOT propagate to existing work specifications in that group. Any other changes will overwrite changes you may have made on individual work specifications.