from django.conf import settings


def version_string(_):
    return {'VERSION_STRING': settings.VERSION_STR}
