"""
Django settings for ldvspec project.
"""
import logging
import os
from pathlib import Path

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent

# SECURITY WARNING: keep the secret key used in production secret!
try:
    SECRET_KEY = os.environ["SECRET_KEY"]
except:
    SECRET_KEY = "django-insecure-2x)iay_r&tj0_d8+z0+kl7ii2oz#048sh^ebfo(q#wmimxob!_"

# SECURITY WARNING: don't run with debug turned on in production!
try:
    DEBUG = os.environ["DEBUG"] == "True"
except:
    DEBUG = False

ALLOWED_HOSTS = []

# Application definition

INSTALLED_APPS = [
    "lofardata",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    # These are required for ASTRONauth
    "django.contrib.sites",
    "astronauth",  # it is important that astronauth is included before allauth
    "allauth",
    "allauth.account",
    "allauth.socialaccount",
    "allauth.socialaccount.providers.openid_connect",
    "rest_framework",
    "rest_framework.authtoken",
    "corsheaders",
    "django_filters",
    "uws",
    "crispy_forms",
    "widget_tweaks",
    "django_prometheus",
]


MIDDLEWARE = [
    "django_prometheus.middleware.PrometheusBeforeMiddleware",
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "allauth.account.middleware.AccountMiddleware",
    "django_prometheus.middleware.PrometheusAfterMiddleware",
]

ROOT_URLCONF = "ldvspec.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "ldvspec.context_processors.version_string",
            ],
        },
    },
]

WSGI_APPLICATION = "ldvspec.wsgi.application"

# Password validation
# https://docs.djangoproject.com/en/4.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

# Internationalization
# https://docs.djangoproject.com/en/4.0/topics/i18n/

LANGUAGE_CODE = "en-us"

TIME_ZONE = "UTC"

USE_I18N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/4.0/howto/static-files/

STATIC_URL = "/ldvspec/static/"
STATIC_ROOT = os.path.join(BASE_DIR, "static")

# Default primary key field type
# https://docs.djangoproject.com/en/4.0/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    "DEFAULT_PERMISSION_CLASSES": [
        "rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly"
    ],
    "DEFAULT_AUTHENTICATION_CLASSES": [
        "rest_framework.authentication.SessionAuthentication",
        "rest_framework.authentication.TokenAuthentication",
    ],
    "DEFAULT_FILTER_BACKENDS": ("django_filters.rest_framework.DjangoFilterBackend",),
    "DEFAULT_PAGINATION_CLASS": "rest_framework.pagination.PageNumberPagination",
    "PAGE_SIZE": 100,
}

# Recommended to use an environment variable to set the broker URL.
CELERY_BROKER_URL = os.environ.get("CELERY_BROKER_URL", "amqp://guest@localhost:5672")
UWS_WORKERS = ["lofardata.workers.query.Echo"]
# "fixes" the WorkerLostError exception, but it causes another minor issue:
# tasks which were executing when termination occurred had finished before shut down AND they will be invoked again after worker restart.
# hence it is sent unnecessarily twice to ATDB (only 1 is linked to the work-spec)
# check this issue if this has been fixed: https://github.com/celery/billiard/issues/273
CELERY_TASK_ACKS_LATE = True

VERSION_STR = "Version 1.0.0 (Unknown)"
version_file_path = BASE_DIR.parent / "VERSION"
if os.path.exists(version_file_path):
    with open(version_file_path, "r") as f_stream:
        VERSION_STR = f_stream.read().strip()
else:
    logging.warning("Cannot find version file %s", version_file_path)

SDC_HELPDESK_URI = "https://support.astron.nl/jira/servicedesk/customer/portal/7"
SESSION_COOKIE_NAME = "ldv-spec-sessionid"
CSRF_COOKIE_NAME = "ldv-spec-csrftoken"
CSRF_TRUSTED_ORIGINS = ["https://sdc-dev.astron.nl", "https://sdc.astron.nl"]

# ASTRONAuth
SOCIALACCOUNT_PROVIDERS = {
    "openid_connect": {
        "SERVERS": [
            {
                "id": "keycloak",
                "name": "Keycloak",
                "server_url": os.getenv(
                    "KEYCLOAK_URL", "https://keycloak-sdc.astron.nl"
                )
                + "/realms/"
                + os.getenv("KEYCLOAK_REALM", "SDC")
                + "/.well-known/openid-configuration",
                "APP": {
                    "client_id": os.getenv("KEYCLOAK_CLIENT_ID", "LDVSPEC-LOCAL"),
                    "secret": os.getenv("KEYCLOAK_CLIENT_SECRET"),
                },
                "SCOPE": ["openid", "profile", "email"],
            }
        ]
    },
}

# Disable signing up for local accounts
SOCIALACCOUNT_ONLY=True
ACCOUNT_EMAIL_VERIFICATION='none'

SITE_ID = int(os.environ.get("SITE_ID", 1))
LOGIN_REDIRECT_URL = os.environ.get("LOGIN_REDIRECT_URL", "/ldvspec/")
LOGOUT_REDIRECT_URL = os.environ.get("LOGOUT_REDIRECT_URL", "/ldvspec/")

# Trust headers from the reverse Proxy
USE_X_FORWARDED_HOST = True
SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")

# User must be logged in and staff to view profiler pages
SILKY_AUTHENTICATION = True
SILKY_AUTHORISATION = True
ENABLE_PROFILING = False

# Look and feel
CRISPY_TEMPLATE_PACK = "bootstrap4"
