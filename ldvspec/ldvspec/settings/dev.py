from ldvspec.settings.base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEV = True
DEBUG = True

ALLOWED_HOSTS = ["*"]
CORS_ORIGIN_ALLOW_ALL = True

DATABASES = {
    'default': {
         'ENGINE': 'django.db.backends.postgresql_psycopg2',
         'USER': 'postgres',
          'PASSWORD': 'secret', # docker container
          #'PASSWORD' : 'postgres', # bare metal
         'NAME': 'ldv-spec-db',
         'HOST': 'localhost',
         'PORT': '5433',  # docker container
         #'PORT': 5432 # bare metal
    },
}

# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = []

# bypass celery async workers
#CELERY_TASK_ALWAYS_EAGER=True

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.PyMemcacheCache',
        'LOCATION': 'localhost:11211',
    }
}

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'http')