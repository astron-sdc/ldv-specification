import os

from ldvspec.settings.base import *

DEV = True
DEBUG = True

ALLOWED_HOSTS = ["*"]
CORS_ORIGIN_ALLOW_ALL = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'USER': os.environ.get("POSTGRES_USER", "postgres"),
        'PASSWORD': os.environ.get("POSTGRES_PASSWORD", "secret"),
        'NAME': os.environ.get("DATABASE_NAME", "ldv-spec-db"),
        'HOST': os.environ.get("DATABASE_HOST_SERVER", "localhost"),
        'PORT': os.environ.get("DATABASE_PORT", "5433")
    },
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.PyMemcacheCache',
        'LOCATION': f'{os.environ.get("CACHE_HOST_SERVER", "localhost")}:{os.environ.get("CACHE_HOST_PORT", "11211")}',
    }
}

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'http')