from ldvspec.settings.dev import  *

ENABLE_PROFILING = True
INSTALLED_APPS += ['silk',]
MIDDLEWARE += ['silk.middleware.SilkyMiddleware',]