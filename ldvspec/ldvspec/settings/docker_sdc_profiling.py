from ldvspec.settings.docker_sdc import *

ENABLE_PROFILING = True
INSTALLED_APPS += ['silk',]
MIDDLEWARE += ['silk.middleware.SilkyMiddleware',]